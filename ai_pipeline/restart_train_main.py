import os
import time
from datetime import datetime
try:
    with open("./AiPipeline.log") as f:
        logs = f.readlines()
        last_log = logs[-1]
        dt = last_log[5:24]
        dt_object = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
        last_time = datetime.timestamp(dt_object)
    print("Last log ===>", last_log)    
except:
    raise BaseException("There is no log.")

def restart(interval = 1800, last_log = last_log, last_time = last_time):
    """
    每 interval 分鐘檢查log，第一次檢查是看log最後一筆的時間，第二次以後是看上一次檢查的時間。
    檢查時，只要當前最後一筆log與 interval 分鐘前的最後一筆log相同就重新執行auto_train_main.py
    """
    now_time = time.time()
    if int(now_time) >=  int(last_time) + interval: # 30分鐘
        with open("./AiPipeline.log") as f:
            logs = f.readlines()
            now_log = logs[-1]
        if now_log == last_log:
            print("the program restart from _restart.py")
            last_time = now_time
            os.system("python3 auto_train_main.py")
            return True, now_log
        else:
            last_time = now_time
            last_log = now_log
            print("Last log ===>", last_log) 
    else:
        print("Continue listening")
        return False, last_log
        
if __name__ == "__main__":
    restart_time = 1
    while(restart_time <= 5): # 最多重新執行5次
        res, last_log, last_time = restart(1800, last_log, last_time) 
        if res:
            restart_time+=1
        time.sleep(10)
        
