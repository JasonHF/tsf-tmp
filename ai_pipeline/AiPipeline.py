import logging
import numpy as np
import pandas as pd
from utils._ai_config import get_default_config,set_config
from utils._check import _check_port
from utils._dimension_reduction import *
from utils._model_train import *
from utils.dl_pipeline import DLPipeline
from utils.ml_pipeline import MLPipeline

ai_logging = logging.getLogger('ai_pipeline')

class AiPipeline:
    def __init__(self,config):
        self.config = set_config(config, get_default_config())
        port_used = _check_port(self.config)
        if port_used:
            raise ValueError("Config 'dask_port' in used.")
        ai_logging.info(f"{self.config}")

    def gen_dd_data_transform(self,data,dd_param,drop_original_feature=True):
        ai_logging.info('dimension reduction transform :{}'.format(str(list(dd_param.keys()))))
        if 'high_correlation' in dd_param.keys():
            col_list = dd_param['high_correlation'].copy()
            transform_data = data.loc[:,col_list]
        dd_transform_data_list = []
        if 'LDA' in dd_param.keys():
            lda_trans_test = dd_param['LDA'].transform(transform_data.values)
            lda_test = pd.DataFrame(lda_trans_test,columns=['lda_{}'.format(i) for i in range(lda_trans_test.shape[1])])
            dd_transform_data_list += [lda_test]
        if 'PCA' in dd_param.keys():
            pca_trans_test = dd_param['PCA'].transform(transform_data.values)
            pca_data_test = pd.DataFrame(pca_trans_test,columns=['pca_{}'.format(i) for i in range(pca_trans_test.shape[1])])
            dd_transform_data_list += [pca_data_test]
        if 'tsne' in dd_param.keys():
            embedding_test = dd_param['tsne'].transform(transform_data.values)
            tsne_data_test = pd.DataFrame(embedding_test,columns=['tsne_{}'.format(i) for i in range(embedding_test.shape[1])])
            dd_transform_data_list += [tsne_data_test]
        if drop_original_feature is False:
            dd_transform_data_list += [data]
        dd_transform_data = pd.concat(dd_transform_data_list,axis=1)
        return dd_transform_data

    def gen_dd_data(self,train_data,train_data_y,test_data,drop_original_feature=True):
        self.dd_method = self.config['DD_METHOD'].copy()
        ai_logging.info('dimension reduction :{}'.format(str(list(self.dd_method.keys()))))
        dd_param = {}
        if 'high_correlation' in self.dd_method.keys():
            self.dd_method['high_correlation']['data'] = train_data.copy()
            high_corr_data, col_list = high_corr_reduction(**self.dd_method['high_correlation'])
            train_data = train_data.loc[:,col_list]
            test_data = test_data.loc[:,col_list]
            dd_param['high_correlation'] = col_list
        dd_train_data_list = []
        dd_test_data_list = []
        if 'LDA' in self.dd_method.keys():
            lda_train, lda_test, lda = LDA_reduction(train_data,train_data_y,test_data,**self.dd_method['LDA'])
            dd_train_data_list += [lda_train]
            dd_test_data_list += [lda_test]
            dd_param['lda'] = lda
        if 'PCA' in self.dd_method.keys():
            pca_data_train,pca_data_test, pca = PCA_reduction(train_data,test_data,**self.dd_method['PCA'])
            dd_train_data_list += [pca_data_train]
            dd_test_data_list += [pca_data_test]
            dd_param['PCA'] = pca
        if 'tsne' in self.dd_method.keys():
            tsne_data_train, tsne_data_test, embedding_fit = tsne_reduction(train_data,test_data,**self.dd_method['tsne'])
            dd_train_data_list += [tsne_data_train]
            dd_test_data_list += [tsne_data_test]
            dd_param['tsne'] = embedding_fit
        if drop_original_feature is False:
            dd_train_data_list += [train_data]
            dd_test_data_list += [test_data]
        dd_train_data = pd.concat(dd_train_data_list,axis=1)
        dd_test_data = pd.concat(dd_test_data_list,axis=1)
#         self.train_data = dd_train_data.copy()
#         self.test_data = dd_test_data.copy()
        return dd_train_data,dd_test_data,dd_param
        
    def fit(self,x_train, y_train, x_val, y_val,method,target_directory,category_size=1):
#             retrain_size=RETRAIN_SIZE, retrain_times=RETRAIN_TIMES,\
#             tuned_parameters=AI_PIP_TUNED_PARAMETERS,scoring=AI_PIP_TRAIN_SCORING, \
#             max_train_size=AI_PIP_MAX_TRAIN_SIZE,fit_cconfig):
        if method=='Auto_keras_tsf':
            dl_pipe = DLPipeline(project_type = 'tsf',target_directory=target_directory)
            best_model, best_params, best_score = dl_pipe.fit(
                x_train=x_train, y_train=y_train, x_val=x_val, y_val=y_val, \
                tuned_params=self.config['AI_PIP_TUNED_PARAMETERS']['Auto_keras_tsf'], \
                scoring = self.config['AI_PIP_TRAIN_SCORING'])
        if method=='Auto_sk_tsf':
            ml_pipe = MLPipeline(project_type = 'tsf',target_directory=target_directory)
            best_model, best_params, best_score = ml_pipe.fit(
                x_train=x_train, y_train=y_train, x_val=x_val, y_val=y_val, \
                retrain_size=self.config['RETRAIN_SIZE']*category_size, \
                tuned_params=self.config['AI_PIP_TUNED_PARAMETERS']['Auto_sk_tsf'], \
                scoring=self.config['AI_PIP_TRAIN_SCORING'])
        return best_model, best_params, best_score
        
    def refit(self,method,model, x, y):
        if method=='Auto_keras_tsf':
            dl_pipe = DLPipeline(project_type = 'tsf',best_model=model,params=self.config['AI_PIP_TUNED_PARAMETERS']['Auto_keras_tsf'])
            refit_model = dl_pipe.refit(x=x, y=y)
        if method=='Auto_sk_tsf':
            ml_pipe = MLPipeline(project_type = 'tsf',best_model=model)
            refit_model = ml_pipe.refit(x=x, y=y)
        return refit_model
    
    def predict(self,method,model,x,predict_length):
        if method=='Auto_keras_tsf':
            dl_pipe = DLPipeline(project_type = 'tsf',best_model=model,params=self.config['AI_PIP_TUNED_PARAMETERS']['Auto_keras_tsf'])
            lookback = model.layers[0].input_shape[0][1]
            y_pred = dl_pipe.predict(x[-(lookback+predict_length):].reset_index(drop=True))
        if method=='Auto_sk_tsf':
            ml_pipe = MLPipeline(project_type = 'tsf',best_model=model)
            y_pred = ml_pipe.predict(x[-predict_length:].reset_index(drop=True))
        return list(y_pred)