DEFAULT_LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'normal': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        },
        'simple': {
            'format': '%(levelname)s - %(message)s'
        },
        'hf': { # hf ding de
            'format': '%(levelname)s:%(asctime)s:%(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'normal',
        },
        'ai_file_handler': {
            'class': 'logging.FileHandler',
            'filename': 'AiPipeline.log',
            'formatter': 'hf',
        },
#         'fg_file_handler': {
#             'class': 'logging.FileHandler',
#             'filename': 'FeatureGenerator.log',
#             'formatter': 'hf',
#         },
    },
    'loggers': {
        'ai_pipeline': {
            'handlers': ['ai_file_handler'],
            'level': 'INFO',
#             'propagate': True,
        },
#         'feature_generator': {
#             'handlers': ['fg_file_handler'],
#             'level': 'INFO',
#             'propagate': True,
#         },
    },
}
