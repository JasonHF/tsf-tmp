from datetime import datetime
from math import factorial
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as stats
import scipy.signal as signal
from statsmodels.tsa.stattools import adfuller, acf, pacf
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf

# 1. 資料遺失率
def missing_ratio(x, time):
    # 最新與最舊資料的間隔
    time_delta = (datetime.strptime(max(time),"%Y-%m-%dT%H:%M:%S") - datetime.strptime(min(time),"%Y-%m-%dT%H:%M:%S")).days / 30
    return 1 - (len(y) / int(time_delta))

# 2. 平穩性檢驗
def stationary_test(x):
    test = adfuller(x,autolag='AIC')
    # 虛無假設：時間序列存在單位根(不具平穩性)
    # ADF檢驗結果：T統計量、p值、延遲、樣本數、臨界點(99,95,90%信心水準)
    title = ['Test Statistic','p-value','#Lags Used','Number of Observations Used', 'Critical Value']
    df_result = pd.DataFrame(dict(zip(title,test)))
    df_result = df_result.sort_values("Critical Value")
    return df_result

# 3. 排序熵
def _embed(x, order=3, delay=1):
    N = len(x)
    Y = np.empty((order, N - (order - 1) * delay))
    for i in range(order):
        Y[i] = x[i * delay:i * delay + Y.shape[1]]
    return Y.T

def permutation_entropy(x, order=3, delay=1, normalize=True):
    x = np.array(x)
    hashmult = np.power(order, np.arange(order))
    # Embed x and sort the order of permutations
    sorted_idx = _embed(x, order=order, delay=delay).argsort(kind='quicksort')
    # Associate unique integer to each permutations
    hashval = (np.multiply(sorted_idx, hashmult)).sum(1)
    # Return the counts
    _, c = np.unique(hashval, return_counts=True)#【2】
    # Use np.true_divide for Python 2 compatibility
    p = np.true_divide(c, c.sum())
    pe = -np.multiply(p, np.log2(p)).sum()#【3】
    if normalize:
        pe /= np.log2(factorial(order))
    return pe

# 4. 週期性觀察
def seasonality(x, order = 2):
    ORDER = order
    # signal.argrelextrema
    # 局部最大值
    local_max_index = list(signal.argrelextrema(x, np.greater,order=ORDER)[0])
    # 山峰時間點
    peak_mode_result = stats.mode([local_max_index[i] - local_max_index[i-1] for i in range(1, len(local_max_index))])
    try:
        peak = peak_mode_result[0][0]
        peak_cnt = peak_mode_result[1][0]
    except:
        peak = -1
        peak_cnt = 0
    
    # 局部最小值
    local_min_index = list(signal.argrelextrema(x, np.less,order=ORDER)[0])
    # 山谷時間點
    valley_mode_result = stats.mode([local_min_index[i] - local_min_index[i-1] for i in range(1, len(local_min_index))])
    try:
        valley = valley_mode_result[0][0]
        valley_cnt = valley_mode_result[1][0]
    except:
        valley = -1
        valley_cnt = 0
    return peak, peak_cnt, valley, valley_cnt

# 5. 自相關函數
def autocorrelation(x):
    coef, qstat, pvalues = acf(x, qstat = True, nlags = 10, fft=False)
    lags = np.argwhere(pvalues<0.05) # 95%信心水準，顯著的滯後階數
    return lags.reshape(-1).tolist() if len(lags)>0 else [-1]

# 6. 偏自相關函數
def autocorrelation_partial(x):
    coef, qstat = pacf(x, nlags = 10, alpha = 0.05) # 自相關係數與信心區間下上界
    lags = []
    for idx in range(10):
        if (coef[idx]>qstat[idx][1]) or (coef[idx] < qstat[idx][0]):
            lags.append(idx)
    return lags.reshape(-1).tolist() if len(lags)>0 else [-1]

# 7. 需求模式
def calc_mean(amount_list):
    amount_array = np.asarray(amount_list)
    my_mean = np.mean(amount_array)
    return float(my_mean)
    
def calc_var(amount_list):
    amount_array = np.asarray(amount_list)
    var = np.std(amount_array)
    return float(var)

def calc_cv2(amount_list):
    amount_array = np.asarray(amount_list)
    my_mean = np.mean(amount_array)
    if my_mean == 0:
        return 0
    var = np.std(amount_array)
    mean = np.mean(amount_array)
    cv2 = np.square(var / mean)
    return float(cv2)
    
def calc_adi(amount_list):
    amount_array = np.array(amount_list)
    adi = 0
    head = 0
    denominator = 0
    for idx, amount in enumerate(amount_list):
        if amount == 0:
            continue
        else:
            denominator+=1
            adi += (idx - head)  
            head = idx 
    if denominator-1==0:
        return 0
    else: 
        return float(adi)/float(denominator-1)

def calc_material_type(amount_list):
    cv2 = calc_cv2(amount_list)
    adi = calc_adi(amount_list)
    if cv2 <=0.49 and adi <= 1.32:
        return 'smooth'
    elif cv2 > 0.49 and adi <= 1.32:
        return 'erratic'
    elif cv2 <=0.49 and adi > 1.32:
        return 'intermittent'
    else:
        return 'lumpy'
        
# 整合上列所有函數，生成結果
def predictable_result(x):
    binary_value = True if len(set(x))<=2 else False
    zero_rate = round(len(x[x==0])/len(x),3)
    same_value_rate = round(np.unique(x, return_counts=True)[1][0]/len(x),3)
    stationary = True if stationary_test(x)['p-value'][0] < 0.05 else False
    std = round(np.std(x), 3)
    entropy = round(permutation_entropy(x),3) if np.std(x)!=0 else 0
    peak, peak_cnt, valley, valley_cnt = seasonality(x)
    peak_rate, valley_rate = round(peak_cnt/len(x),3), round(valley_cnt/len(x),3)
    lags_rank = max(autocorrelation(x))
    demand_pattern = calc_material_type(x)
    # 分數若沒有被扣到零就是可預測的
    predictable_score = 1.0
    if binary_value == True:
        predictable_score-=1.1
    if same_value_rate > 0.9:
        predictable_score-=0.5
    if demand_pattern != "smooth":
        predictable_score-=0.4
    if stationary == False:
        predictable_score-=0.2
    if peak == valley == -1:
        predictable_score-=0.2
    if peak_rate == valley_rate == 0:
        predictable_score-=0.2
    if lags_rank == -1:
        predictable_score-=0.2
    predictable = 1 if predictable_score > 0 else 0
    return zero_rate, same_value_rate, std, stationary, entropy, peak, peak_rate, valley, valley_rate, lags_rank, demand_pattern, predictable