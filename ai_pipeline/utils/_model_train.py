import xgboost as xgb
from sklearn.model_selection import TimeSeriesSplit, GridSearchCV
import numpy as np

def fit_cv_model(method,retrain_size,retrain_times,tuned_parameters,data_X,data_y,scoring,max_train_size=None):
    """
    Not used now.
    """
    if max_train_size:
        tscv = TimeSeriesSplit(n_splits=retrain_times, test_size=retrain_size,max_train_size=max_train_size)
    else :
        tscv = TimeSeriesSplit(n_splits=retrain_times, test_size=retrain_size)
    train_X = data_X.values
    train_y = data_y.values
    feature_list = data_X.columns
    cv = GridSearchCV(method(), tuned_parameters, scoring = scoring, refit=True, cv=tscv,return_train_score=True)
    cv.fit(train_X, train_y)
    best_params = cv.best_params_.copy()
    model = method(**best_params)
    model.fit(train_X, train_y)
    feature_importance_dict = {}
    for feature_nm,feature_importance in zip(feature_list, model.feature_importances_):
        feature_importance_dict[feature_nm]=feature_importance
    return model,cv,feature_importance_dict