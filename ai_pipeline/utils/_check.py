import socket

def _check_port(config) -> bool:
    port = config['AI_PIP_TUNED_PARAMETERS']['Auto_sk_tsf']['dask_port']
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(('localhost', port)) == 0
