import autokeras as ak
import logging
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras import metrics as TF_METRICS

ai_logging = logging.getLogger('ai_pipeline')

class DLPipeline:
    def __init__(self, project_type = 'tsf', target_directory = "./", best_model = None, params = {}):
        self._project_type = project_type
        
        self.target_directory = target_directory
        self._auto_model = None
        self.best_model = best_model
        if (self.best_model != None) & (self._project_type == "tsf"):
            if len(self.best_model.layers[0].input_shape[0]) > 2:
                self.lookback = self.best_model.layers[0].input_shape[0][1]
            else:
                raise BaseException("The shape of input layer should be more then 2 dim")
        self.params = params
        
    def _set_parameters(self, tuned_params, scoring):
        
        # 檢查params
        try:
            tuner = tuned_params["tuner"]
            lookback = tuned_params["lookback"]
            max_trials  = tuned_params["max_trials"]
            epochs = tuned_params["epochs"]
        except:
            tuner = "greedy"
            lookback = 1
            max_trials = 100
            epochs = 1000
        
        # 檢查scoring
        scoring = scoring.replace("_", "").lower()
        loss_function_name = {
        "meanabsoluteerror":TF_METRICS.MeanAbsoluteError(),
        "mae":TF_METRICS.MeanAbsoluteError(),
        "meansquarederror":TF_METRICS.MeanSquaredError(),
        "mse":TF_METRICS.MeanSquaredError(),
        "meansquaredlogerror":TF_METRICS.MeanSquaredLogarithmicError(),
        "msle":TF_METRICS.MeanSquaredLogarithmicError(),
        "rootmeansquarederror":TF_METRICS.RootMeanSquaredError(),
        "rmse":TF_METRICS.RootMeanSquaredError()
        }
        if scoring in loss_function_name:
            scoring = loss_function_name[scoring]
        else:
            raise BaseException("Scoring function is not supported")
            
        self.params = {"tuner":tuner, "lookback":lookback, "max_trials":max_trials, "epochs":epochs, "scoring":scoring}
    
    def _set_automodel(self, tuner = 'bayesian', metrics = ['mse'], max_trials = 10, lookback = None):
        if self._project_type == 'tsf':
            automodel = ak.TimeseriesForecaster(
                            lookback=lookback,
                            predict_from=1, 
                            predict_until=None, 
                            max_trials=max_trials, 
                            max_model_size = 1 * 10e10,
                            tuner=tuner,
                            metrics = metrics,
                            objective="val_loss",
                            overwrite=True,
                            directory = self.target_directory,
                            project_name = "DL")
        else:
            automodel = None
            raise BaseException(f"Please check project_type, {self._project_type} is not supported")
            
        return automodel
    
    def _shift_window(self, x, y = []):
        """
        for tsf exported model, we should reshape x to (size, lookback, feature_dim)
        """
        x_lookbacks = []
        y_lookbacks = []

        sample_size, feature_size = x.shape[0], x.shape[1]
        for shift_i in range(sample_size - self.lookback):
            x_lookbacks.append(x.shift(-shift_i).to_numpy()[:self.lookback])
            if len(y) > 0 :
                y_lookbacks.append(y.shift(-shift_i).to_numpy()[:self.lookback])

        x = np.array(x_lookbacks).reshape((sample_size -self.lookback, self.lookback, feature_size))
        if len(y) > 0 :
            y = np.array(y_lookbacks).reshape((sample_size - self.lookback, self.lookback, 1))
        return x,y
    
    def fit(self, x_train, y_train, x_val, y_val, tuned_params, scoring = 'mse'):     
        # 設定automodel
        self._set_parameters(tuned_params, scoring)
        automodel = self._set_automodel(metrics = [self.params["scoring"]], 
                                        tuner = self.params["tuner"], 
                                        max_trials = self.params["max_trials"], 
                                        lookback = self.params["lookback"])
        
        # 先放進train和f全部val做NAS和HPO
        cb_list = [tf.keras.callbacks.EarlyStopping(patience=(self.params["epochs"]//10)+1)]
        automodel.fit(x=x_train, y=y_train, validation_data=(x_val, y_val), 
                      batch_size=self.params["lookback"], epochs=self.params["epochs"], callbacks = cb_list) #bs必須是lb的因數
        # 暫存automodel
        self._auto_model = automodel
        self.lookback = self._auto_model.lookback
        # 取出keras模型
        best_model = automodel.export_model()
        self.best_model = best_model
        # 取出最佳超參數
        best_hp = automodel.tuner.get_best_hyperparameters()[0].values
        
        # 算出最佳模型表現
        x_val_tmp = pd.concat([x_train[-self.lookback:], x_val], ignore_index=True)
        x_val_tmp = self._shift_window(x_val_tmp)
        y_val_pred = self.best_model.predict(x_val_tmp)
        best_model_scoring = self.params["scoring"](y_val.to_numpy().flatten(), y_val_pred.flatten()).numpy()
        ai_logging.info(f"best_model_scoring: {best_model_scoring}")
        return best_model, [best_hp], best_model_scoring
    
    def refit(self, x, y):
        if self.best_model==None:
            raise BaseException("One should fit first")
        elif self.lookback != self.params["lookback"]:
            raise BaseException(f"The input shape of model is not equal to the {self.params['lookback']}")
        else:
            if self._project_type == "tsf":
                x_new, y_new = self._shift_window(x, y)
            refit_model = self.best_model
            cb_list = [tf.keras.callbacks.EarlyStopping(patience=(self.params["epochs"]//10)+1, monitor="loss")]
            refit_model.fit(x=x_new, y=y_new, batch_size=self.lookback, epochs = self.params["epochs"], callbacks = cb_list)
            self.best_model = refit_model
            return refit_model
    
    def predict(self, x):
        if self.best_model==None:
            raise BaseException("One should fit first")
        else:
            if self._project_type == "tsf":
                x_new, _ = self._shift_window(x)
            y_pred = self.best_model.predict(x_new)
            return y_pred.flatten()