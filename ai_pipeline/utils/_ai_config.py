def set_config(custom_config, default_config):
    if custom_config == {}:
        custom_config = default_config
        return custom_config
    
    for k, v in custom_config.items():
        if k in default_config:
            default_config[k] = v
    custom_config = default_config
    return custom_config

def get_default_config():
    default_config = {}
    #model_train.py
    default_config['DROP_ORIGINAL_FEATURE'] = True
    default_config['RETRAIN_SIZE'] = 10
    default_config['AI_PIP_MAX_TRAIN_SIZE'] = 500
    default_config['AI_PIP_TRAIN_SCORING'] = 'mse'
    default_config['AI_PIP_TUNED_PARAMETERS'] = {
        'Auto_keras_tsf':{
            "tuner": 'greedy', #random, grid, bayesian,
            "lookback": 1,
            "max_trials": 10,
            "epochs": 20
        },
        'Auto_sk_tsf':{
            "time_left_for_this_task": 3600,
            "per_run_time_limit": 1200,
            "memory_limit": None,
            "ensemble_size": 10,
            "ensemble_nbest": 10,
            "n_jobs": 16,
            "exclude_preprocessors": ["select_rates_regression"],
            "delete_tmp_folder_after_terminate": False,
            
            # This param is not in vallina autosklearn, will be uesd to create dask_client and pop it.  
            "dask_port":30678,
        }
    }
    default_config['AI_PIP_ESTIMATOR_LIST'] = ['Auto_keras_tsf','Auto_sk_tsf']
    default_config['DD_METHOD'] = {
        'high_correlation':{
            'threshold':0.9
        },
        'LDA':{
            'threshold':0.9
        },
        'PCA':{
            'n_components':0.9,'svd_solver':'auto'
        },
        'tsne':{
            "n_components": 2,
            "perplexity": 30,
            "metric": "euclidean",
            "n_jobs": 8,
            "random_state": 42,
            "verbose": False
        }
    }
    return default_config
