import os
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from openTSNE import TSNE
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

def LDA_reduction(train_data,train_data_y,test_data=None,threshold=0.9):
    for n_components in range(1,len(train_data.columns),5):
        lda = LinearDiscriminantAnalysis(n_components = n_components)
        lda.fit(train_data.values,train_data_y.values)
        if np.sum(lda.explained_variance_ratio_)>threshold:
            break
    lda_trans_train = lda.transform(train_data.values)
    lda_train = pd.DataFrame(lda_trans_train,columns=['lda_{}'.format(i) for i in range(lda_trans_train.shape[1])])
    lda_test = pd.DataFrame()
    if test_data is not None:
        lda_trans_test = lda.transform(test_data.values)
        lda_test = pd.DataFrame(lda_trans_test,columns=['lda_{}'.format(i) for i in range(lda_trans_test.shape[1])])
    return lda_train, lda_test, lda

def high_corr_reduction(data,threshold=0.9):
    col_list = data.columns.to_list()
    corr_df = data.corr()
    high_corr_df = corr_df.unstack().sort_values(ascending=False).drop_duplicates()
    corr_mean_dict = corr_df.mean().to_dict()
    for i in high_corr_df[abs(high_corr_df)>threshold].index:
        if (i[0]!=i[1])&(i[0] in col_list)&(i[1] in col_list):
            if corr_mean_dict[i[0]]>corr_mean_dict[i[1]]:
                col_list.remove(i[0])
            else:
                col_list.remove(i[1])
    high_corr_data = data.loc[:,col_list]
    return high_corr_data, col_list

def PCA_reduction(train_data,test_data=None,n_components=0.9,svd_solver='auto'):
    pca = PCA(n_components = n_components,svd_solver=svd_solver)
    pca.fit(train_data.values)
    pca_trans_train = pca.transform(train_data.values)
    pca_data_train = pd.DataFrame(pca_trans_train,columns=['pca_{}'.format(i) for i in range(pca_trans_train.shape[1])])
    pca_trans_test = pca.transform(test_data.values)
    pca_data_test = pd.DataFrame(pca_trans_test,columns=['pca_{}'.format(i) for i in range(pca_trans_test.shape[1])])
    return pca_data_train,pca_data_test, pca

def tsne_reduction(train_data,test_data=None,n_components=2,perplexity=30,metric='euclidean',n_jobs=8,random_state=42,verbose=False):
    
    train_X = train_data.values
    tsne = TSNE(n_components=n_components,perplexity=n_components,metric=metric,n_jobs=n_jobs,random_state=random_state,verbose=verbose)
    embedding_fit = tsne.fit(train_X)
    embedding_train = embedding_fit.transform(train_X)
    tsne_data_train = pd.DataFrame(embedding_train,columns=['tsne_{}'.format(i) for i in range(embedding_train.shape[1])])
    tsne_data_test = pd.DataFrame()
    if test_data is not None:
        test_X = test_data.values
        embedding_test = embedding_fit.transform(test_X)
        tsne_data_test = pd.DataFrame(embedding_test,columns=['tsne_{}'.format(i) for i in range(embedding_test.shape[1])])
    return tsne_data_train, tsne_data_test, embedding_fit