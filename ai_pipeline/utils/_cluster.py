# import datetime
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import time
# Algorithms
from scipy.cluster.hierarchy import linkage
import sklearn.cluster
from sklearn.preprocessing import MinMaxScaler

ai_logging = logging.getLogger('ai_pipeline')

def time_series_cluster(whole_df, column_id, column_time, column_target, method="AUTO", params=None, return_info=False):
    if method.upper() == "EACH":
        return { k: v for k, v in zip(whole_df[column_id], whole_df[column_id])}
    elif method.upper() == "ALL":
        return { k: 0 for k in whole_df[column_id]}
    
    new_df = whole_df.pivot(index=column_id, columns=column_time, values=column_target)
    time_series = new_df.to_numpy()
    for i in range(len(time_series)):
        scaler = MinMaxScaler()
        time_series[i] = scaler.fit_transform(time_series[i].reshape(-1, 1)).flatten()
    
    use_hrk = False
    if method.upper() == "H" or method.upper() == "AUTO":
        use_hrk = True
    else:
        cluster = get_cluster_algorithm(method, params, time_series.shape[0])
        if not cluster:
            raise ValueError(f"Unrecognize method name: '{method}'")
    
    start = time.time()
    labels = None
    if use_hrk:
        hrk_method = 'weighted'
        hrk_metric = 'cosine'
        hrk_thres = 0.7
        hrk_res = linkage(time_series, method=hrk_method, metric=hrk_metric)
        labels = parse_hrk(hrk_res, time_series.shape[0], hrk_thres)
        l = len(np.unique(labels))
        while not (l >= 3 and l <= 15):
            if l < 3:
                hrk_thres *= 0.9
            elif l > 15:
                hrk_thres *= 1.1
            labels = parse_hrk(hrk_res, time_series.shape[0], hrk_thres)
            l = len(np.unique(labels))
            if hrk_thres < 0.001:
                break
    else:
        cluster.fit(time_series)
        if hasattr(cluster, "labels_"):
            labels = cluster.labels_.astype(int)
        else:
            labels = cluster.predict(time_series)
    end = time.time()
    
    category, frequency = np.unique(labels, return_counts=True)
    label_dict = { k: v for k, v in zip(new_df.index, labels) }
    
    if return_info:
        return label_dict, { 'Algorithm': type(cluster), 'Category Count': len(category), 
                            'Data Count': frequency, 'Data Shape': np.array(time_series).shape, 'Time Cost': time_delta(end-start) }
    
    ai_logging.info(f"Cluster Count: {len(category)}\tData Count: {frequency}\tData Shape: {np.array(time_series).shape}")
    ai_logging.info(f"Totally Time Cost({method}): {time_delta(end-start)}")
    return label_dict

def get_cluster_algorithm(method, params, ts_length):
    method = method.upper()
    
    if ts_length <= 10:
        n_clusters = ts_length
        min_samples = 1
    elif ts_length > 10:
        n_clusters = 10
        min_samples = 10
    
    default = {
        "quantile": 0.3,
        "eps": 0.3,
        "damping": 0.9,
        "preference": -200,
        "n_neighbors": 10,
        "n_clusters": n_clusters,
        "min_samples": min_samples,
        "xi": 0.05,
        "min_cluster_size": 0.1,
        "metric": 'euclidean',
    }
    '''
    Metric
        1. Agglomerative
            - metric list: “euclidean”, “l1”, “l2”, “manhattan”, “cosine”, or “precomputed”.
        2. Birch
            - metric list: None.
        3. DBSCAN
            - metric list: 'cityblock’, ‘cosine’, ‘euclidean’, ‘l1’, ‘l2’, ‘manhattan’.
        4. MiniBatchKMeans
            - metric list: None.
        5. OPTICS
            - metric list: Same as DBSCAN.
        6. Spectral
            - metric list: None.
            
    '''
    params = copy_params(default, params)
    # A B D K O S S 
    cluster_algo = None
    if method == "A" or method == "Agglomerative".upper() or method == "AgglomerativeClustering".upper():
        cluster_algo = sklearn.cluster.AgglomerativeClustering(n_clusters=params["n_clusters"],
                                                               affinity=params["metric"],
                                                               linkage="average")
        
    elif method == "B" or method == "BIRCH":
        cluster_algo = sklearn.cluster.Birch(n_clusters=params["n_clusters"])
        
    elif method == "D" or method == "DBSCAN":
        cluster_algo = sklearn.cluster.DBSCAN(eps=params["eps"],
                                             metric=params["metric"])
    
    elif method == "K" or method == "MiniBatchKMeans".upper() or method == "KMEANS":
        cluster_algo = sklearn.cluster.MiniBatchKMeans(n_clusters=params["n_clusters"])
        
    elif method == "O" or method == "OPTICS":
        cluster_algo = sklearn.cluster.OPTICS(min_samples=params["min_samples"],
                                              metric=params["metric"],
                                              xi=params["xi"],
                                              min_cluster_size=params["min_cluster_size"],)
        
    elif method == "S" or method == "Spectral".upper() or method == "SpectralClustering".upper():
        cluster_algo = sklearn.cluster.SpectralClustering(n_clusters=params["n_clusters"], 
                                                          eigen_solver="arpack", 
                                                          affinity="nearest_neighbors")
    else:
        pass
    return cluster_algo

def copy_params(default, custom):
    if not custom:
        pass
    else:
        for k, v in custom.items():
            default[k] = v
    return default

def time_delta(total_time):
    tt = total_time
    time_str = ""
    seconds = round(tt%60, 3)
    time_str = (str(seconds) + 'sec') if seconds > 0 else ''
    minutes = int(tt/60%60)
    time_str = (str(minutes) + 'min ' + time_str) if minutes > 0 else time_str
    hour = int(tt/3600%24)
    time_str = (str(hour) + 'hr ' + time_str) if hour > 0 else time_str
    day = int(tt/86400)
    time_str = (str(day) + 'day ' + time_str) if day > 0 else time_str
    return time_str

def parse_hrk(hrk_list, ts_len, threshold=0.7, invert_label=True):
    hrk_cols = [ 'left_child', 'right_child', 'score', 'children' ]
    hrk_cols_type = [ int, int, float, int ]
    hrk_df = pd.DataFrame(hrk_list, columns=hrk_cols)
    origin_df = pd.DataFrame(data=[[i, -1, 0, 1] for i in range(ts_len)], columns=hrk_cols)
    hrk_df = pd.concat([origin_df, hrk_df]).reset_index(drop=True)
    for col, tpe in zip(hrk_cols, hrk_cols_type):
        hrk_df[col] = hrk_df[col].astype(tpe)
    
    chd_list = []
    for idx, row in hrk_df.iterrows():
        if int(row[1]) == -1:
            chd_list.append([int(row[0])])
        else:
            chd_list.append(chd_list[int(row[0])] + chd_list[int(row[1])])
    hrk_df['child_list'] = pd.Series(chd_list)
    
    pivot = hrk_df['score'].iloc[hrk_df.shape[0]-1] * threshold
    hrk_df_high = hrk_df[hrk_df['score'] >= pivot]
    
    cluster = []
    ignore = []
    for idx, row in hrk_df_high.iterrows():
        lchd, rchd = int(row[0]), int(row[1])
        lchdin, rchdin = lchd in ignore, rchd in ignore
        if lchdin and rchdin:
            pass
        elif lchdin and not rchdin:
            cluster.append(rchd)
            ignore.append(rchd)
        elif not lchdin and rchdin:
            cluster.append(lchd)
            ignore.append(lchd)
        else:
            cluster.append(idx)
        ignore.append(idx)
    
    result = hrk_df[hrk_df.index.isin(cluster)]['child_list'].tolist()
    if invert_label:
        labels = []
        for each_clu in range(len(result)):
            for ts_id in result[each_clu]:
                labels.append((ts_id, each_clu))
        labels = sorted(labels, key=lambda Q:Q[0])
        return [ label for ts_id, label in labels ]
    return result
