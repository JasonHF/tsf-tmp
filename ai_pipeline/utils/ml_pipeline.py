from autosklearn.regression import AutoSklearnRegressor
import autosklearn.metrics as AS_METRICS
import dask
import dask.distributed
import datetime
import logging
import numpy as np
import os
import pandas as pd
import sklearn
import tempfile

ai_logging = logging.getLogger('ai_pipeline')

class MLPipeline(AutoSklearnRegressor):
    def __init__(self, project_type, target_directory="./", best_model=None):
        self._project_type = project_type
        self.params = {}
        self.target_directory = target_directory
        if best_model:
            self.best_model = best_model
            self.best_params = best_model.get_models_with_weights()
    
    def fit(self, x_train, y_train, x_val, y_val, retrain_size, tuned_params, scoring='mse', verbose=False):
        if verbose:
            print(f"Initializing ...")
        ai_logging.info(f"Initializing ...")
        
        self._set_parameters(tuned_params, scoring)
        scoring = self.params['metric']
        
        retrain_time = int(x_val.shape[0] / retrain_size)
        residual = x_val.shape[0] % retrain_size
        automl = AutoSklearnRegressor(**self.params)
        
        if verbose:
            print(f"Fitting ...")
        ai_logging.info(f"Fitting ...")
        
        automl.fit(x_train, y_train)
        self._close_dask_client()
        y_pred = automl.predict(x_val.loc[:retrain_size-1, :])
        predictions = y_pred
        
        if verbose:
            print(f"Refitting ...")
        ai_logging.info(f"Refitting ...")
        
        ptr = None
        for time_i in range(1, retrain_time):
            ptr = time_i*retrain_size
            automl.refit(pd.concat([x_train, x_val.loc[:ptr-1, :]]).reset_index(drop=True), pd.concat([y_train, y_val.loc[:ptr-1]]).reset_index(drop=True))
            y_pred = automl.predict(x_val.loc[ptr:ptr+retrain_size-1, :])
            predictions = np.append(predictions, y_pred)
        
        if not residual == 0:
            ptr = retrain_time*retrain_size
            automl.refit(pd.concat([x_train, x_val.loc[:ptr-1, :]]).reset_index(drop=True), pd.concat([y_train, y_val.loc[:ptr-1]]).reset_index(drop=True))
            y_pred = automl.predict(x_val.loc[ptr:, :])
            predictions = np.append(predictions, y_pred)
        
        automl.refit(pd.concat([x_train, x_val]).reset_index(drop=True), pd.concat([y_train, y_val]).reset_index(drop=True))
        
        self.best_model = automl
        best_hp = automl.get_models_with_weights()
        best_score = scoring(y_val, predictions) * scoring._sign
        
        if verbose:
            print(f"Done.")
        ai_logging.info(f"Done.")
        
        if verbose:
            print(f"Validation Score: {best_score}")
        ai_logging.info(f"Validation Score: {best_score}")
            
        return self.best_model, best_hp, best_score
    
    def refit(self, x, y):
        if self.best_model:
            self.best_model.refit(x, y)
            return self.best_model
        else:
            raise('Model Not Found. Maybe use .fit() first.')
    
    def predict(self, x):
        if self.best_model:
            return self.best_model.predict(x)
        else:
            raise('Model Not Found. Maybe use .fit() first.')
    
    def _set_parameters(self, tuned_params, scoring='mse'):
        scoring = scoring.lower().replace('_', '')
        loss_func_name = {
            "mae": "mean_absolute_error",
            "meanabsoluteerror": "mean_absolute_error",
            "mse": "mean_squared_error",
            "meansquarederror": "mean_squared_error",
            "msle": "mean_squared_log_error",
            "meansquaredlogerror": "mean_squared_log_error",
            "rmse": "root_mean_squared_error",
            "rootmeansquarederror": "root_mean_squared_error",        
        }
        if scoring in loss_func_name:
            scoring = getattr(AS_METRICS, loss_func_name[scoring])
        else:
            raise('Scoring function name not define.')
        
        tuned_params['time_left_for_this_task'] = tuned_params['time_left_for_this_task'] if 'time_left_for_this_task' in tuned_params else 3600
        tuned_params['per_run_time_limit'] = tuned_params['per_run_time_limit'] if 'per_run_time_limit' in tuned_params else 600
        tuned_params['memory_limit'] = tuned_params['memory_limit'] if 'memory_limit' in tuned_params else None
        tuned_params['n_jobs'] = tuned_params['n_jobs'] if 'n_jobs' in tuned_params else 16
        tuned_params['metric'] = scoring
        tuned_params['tmp_folder'] = os.path.join(self.target_directory, "ML")
        
        port = tuned_params.pop('dask_port', 30678)
        self._create_dask_client(tuned_params['n_jobs'], port)
        tuned_params['dask_client'] = self._dask_client
        self.params = tuned_params
        return

    def _create_dask_client(self, n_jobs, port):
        self._dask_client = dask.distributed.Client(
            dask.distributed.LocalCluster(
                n_workers=n_jobs,
                processes=False,
                threads_per_worker=1,
                scheduler_port=port,
                dashboard_address=port+1,
                # We use the temporal directory to save the
                # dask workers, because deleting workers
                # more time than deleting backend directories
                # This prevent an error saying that the worker
                # file was deleted, so the client could not close
                # the worker properly
                local_directory=tempfile.gettempdir(),
                # Memory is handled by the pynisher, not by the dask worker/nanny
                memory_limit=0,
            ),
            # Heartbeat every 10s
            heartbeat_interval=10000,
        )
        return

    def _close_dask_client(self):
        self._dask_client.shutdown()
        self._dask_client.close()
        del self._dask_client
        self._dask_client = None
        return
 