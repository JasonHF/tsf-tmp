# -*- coding: utf-8 -*-
from AiPipeline import AiPipeline
from utils._cluster import time_series_cluster
from utils._predictable_anaylsis import predictable_result
from utils.logging import DEFAULT_LOGGING
import os
import re
import shutil
import pandas as pd
import numpy as np
from time import time
import joblib
import json
import logging
os.environ["CUDA_VISIBLE_DEVICES"] = "2"
logging.config.dictConfig(config=DEFAULT_LOGGING)
ai_logging = logging.getLogger('ai_pipeline')
    
configs = {
    "train_feature_path": "/home/b00175/TimeSeries/features/Taipower_data_raw_ft_31_24_202202_padding_train.csv",
    "validation_feature_path": "/home/b00175/TimeSeries/features/Taipower_data_raw_ft_31_24_202202_padding_val.csv",
    "report_path" : "./report",
    'COLUMN_TIME': 'tx_dt',
    'COLUMN_TARGET': 'amount',
    'COLUMN_ID': 'category_id',
    "ai_pipeline_config": {
        "TRAIN_CLUSTER_METHOD": "EACH",
        "DROP_ORIGINAL_FEATURE": True,
        "RETRAIN_SIZE": 10,
        "AI_PIP_MAX_TRAIN_SIZE": 500,
        "AI_PIP_TRAIN_SCORING": 'mae',
        "AI_PIP_TUNED_PARAMETERS": {
            'Auto_keras_tsf':{
                "tuner": 'bayesian', #random, grid, bayesian, greedy
                "lookback": 1,
                "max_trials": 1,
                "epochs": 1,
            },
            'Auto_sk_tsf':{
                "time_left_for_this_task": 300,
                "per_run_time_limit": 120,
                "memory_limit": None,
                "ensemble_size": 10,
                "ensemble_nbest": 10,
                "n_jobs": 8,
                "exclude_preprocessors": ["select_rates_regression"],
                "delete_tmp_folder_after_terminate": False,
                # This param is not in vallina autosklearn, will be uesd to create dask_client and pop it.  
                "dask_port":30678,
            }
        },
        "AI_PIP_ESTIMATOR_LIST": [
            'Auto_keras_tsf',
            'Auto_sk_tsf'
        ],
        "DD_METHOD": {
            "high_correlation": {
                "threshold": 0.9
            },
#             "LDA": {
#                 "threshold": 0.9
#             },
            "PCA": {
                "n_components": 0.9,
                "svd_solver": "auto"
            },
            "tsne": {
                "n_components": 2,
                "perplexity": 30,
                "metric": "euclidean",
                "n_jobs": 8,
                "random_state": 42,
                "verbose": False
            }
        }
    }
}

column_time = configs['COLUMN_TIME']
column_target = configs['COLUMN_TARGET']
column_id = configs['COLUMN_ID']
report_path = configs['report_path']
ai_pipeline_config = configs["ai_pipeline_config"]
ai_pip_estimator_list = ai_pipeline_config['AI_PIP_ESTIMATOR_LIST']
cluster_method = ai_pipeline_config["TRAIN_CLUSTER_METHOD"]
df_train = pd.read_csv(configs["train_feature_path"], dtype = {column_id:str}).iloc[:,:]
df_val = pd.read_csv(configs["validation_feature_path"], dtype = {column_id:str}).iloc[:,:]
np.random.seed(10)

def predictable_analysis(data, column_id = column_id, column_target = column_target):
    df = data
    all_cate_id = df[column_id].unique()
    df_output = []
    for _id in all_cate_id:
            x = df[df[column_id] == _id][column_target].to_numpy()
            zero_rate, same_value_rate, std, stationary, entropy, peak, peak_rate, valley, valley_rate, lags_rank, demand_pattern, predictable = predictable_result(x)
            df_output.append([_id, zero_rate, same_value_rate, std, stationary, entropy, peak, peak_rate, valley, valley_rate, lags_rank, demand_pattern , predictable])
    col_names = [column_id, "zero_rate", "same_value_rate", "std", "stationary", "entropy", "peak", "peak_rate", "valley", "valley_rate", "lags_rank", "demand_pattern","predictable"]
    df_output = pd.DataFrame(df_output, columns = col_names)
    df_pass = df_output[df_output["predictable"]==1].reset_index() #高預測性的品類
    df_fail = df_output[df_output["predictable"]==0].reset_index() #低預測性的品類
    return df_pass, df_fail

def gen_group_df(train_data, val_data, column_id, column_time, column_target, method="AUTO"):    
    group_dict = time_series_cluster(train_data, column_id, column_time, column_target, method)
    
    train_data['GROUP_KEY_BY_FAST_AI'] = train_data[column_id].apply(lambda x: group_dict[x])
    val_data['GROUP_KEY_BY_FAST_AI'] = val_data[column_id].apply(lambda x: group_dict[x])
    
    train_group_list = []
    val_group_list = []
    key_list = []
    for _, data in train_data.groupby('GROUP_KEY_BY_FAST_AI'):
        key_list.append(data[column_id].unique().tolist())
        train_group_list.append(data.drop(columns=[column_id, column_time, 'GROUP_KEY_BY_FAST_AI']).reset_index(drop=True))
    for _, data in val_data.groupby('GROUP_KEY_BY_FAST_AI'):
        val_group_list.append(data.drop(columns=[column_id, column_time, 'GROUP_KEY_BY_FAST_AI']).reset_index(drop=True))
    return train_group_list, val_group_list, key_list

def save_final_model(candidate_list,final_model_path):
    score_list = []
    for k in  candidate_list:
        score_list += [k['best_score']]
    argmin_id = np.argmin(score_list)
    # for argmin_id in [0,1]:
    win_candidate = candidate_list[argmin_id].copy()
    final_model = win_candidate['best_model']
    if win_candidate['method']=='Auto_sk_tsf':
        joblib.dump(final_model, "{}/final_model.pkl".format(final_model_path))
    elif win_candidate['method']=='Auto_keras_tsf':
        final_model.save("{}/final_model.h5".format(final_model_path))

    final_param = {}
    final_param['id_set'] = win_candidate['id_set'].copy()
    final_param['method'] = win_candidate['method']
    if win_candidate['method']=='Auto_sk_tsf':
        final_param['best_params'] = str(win_candidate['best_params'])
        final_param['best_score'] = win_candidate['best_score']
    elif win_candidate['method']=='Auto_keras_tsf':
        final_param['best_params'] = win_candidate['best_params'].copy()
        final_param['best_score'] = float(win_candidate['best_score'])
    tf = open("{}/final_param.json".format(final_model_path), "w")
    json.dump(final_param,tf)
    tf.close()
    return final_param,final_model

def save_dd_param(dd_param,final_model_path):
    if 'high_correlation' in dd_param.keys():
        tf = open("{}/high_correlation.json".format(final_model_path), "w")
        json.dump(dd_param['high_correlation'],tf)
        tf.close()
    if 'LDA' in dd_param.keys():
        joblib.dump(dd_param['LDA'], "{}/LDA.pkl".format(final_model_path))
    if 'PCA' in dd_param.keys():
        joblib.dump(dd_param['PCA'], "{}/PCA.pkl".format(final_model_path))
    if 'tsne' in dd_param.keys():
        joblib.dump(dd_param['tsne'], "{}/tsne.pkl".format(final_model_path))

def folder_clean(report_group_path, ai_pip_estimator_list):
    paths = []
    if "Auto_keras_tsf" in ai_pip_estimator_list:
        paths.append(f'{report_group_path}/DL')
    if "Auto_sk_tsf" in ai_pip_estimator_list:
        paths.append(f'{report_group_path}/ML')
    for path in paths:
        strings = os.listdir(path)
        patterns = ['best_model', 'trial_', 'smac3-output'] # 可再添加
        for i in range(len(strings)):
            for pattern in patterns:
                match = re.findall(pattern, strings[i])
                if match!= []:
                    try:
                        shutil.rmtree(os.path.join(path,strings[i]))
                    except OSError as e:
                        print(e)
                    else:
                        print(f"The directory {strings[i]} is deleted successfully")

def checkpoint(report_path):
    start_group = 0
    if os.path.exists(os.path.join(report_path, 'group_set.json')):
        logs = open(os.path.join('.', 'AiPipeline.log'), 'r', encoding='utf-8').readlines()
        target_logs = []
        for log in logs:
            if not len(log.strip()) > 29:
                continue
            if "Group_" in log:
                target_logs.append(log[29:])
        if len(target_logs) > 0:
            last_log = target_logs[-1]
            start_group = int(re.sub("[^0-9]", '', last_log))
            if "End" in last_log:
                start_group += 1
        if os.path.exists(os.path.join(report_path, 'Group_'+str(start_group))):
            shutil.rmtree(os.path.join(report_path, 'Group_'+str(start_group)))
        return True, start_group
    else:
        return False, start_group

def main(df_train,df_val,report_path,column_time,column_target,ai_pipeline_config,column_id=None):
    
    ckpt, start_group = checkpoint(report_path)
    if not ckpt:
        ai_logging.info("------------------start AiPipeline.------------------")
        ai_logging.info(f"Raw data shape: {df_train.shape}")

        # 1.Predictable analysis
        if not cluster_method == "ALL":
            ai_logging.info("start predictable analysis.")
            df_pass, df_fail = predictable_analysis(data = df_train[[column_id, column_time, column_target]], column_id = column_id, column_target = column_target)
            df_train_np = df_train[df_train[column_id].isin(df_fail[column_id])].reset_index(drop=True) # df for excpetion
            df_val_np = df_val[df_val[column_id].isin(df_fail[column_id])].reset_index(drop=True) # df for excpetion
            df_train = df_train[df_train[column_id].isin(df_pass[column_id])].reset_index(drop=True)
            df_val = df_val[df_val[column_id].isin(df_pass[column_id])].reset_index(drop=True)
            ai_logging.info(f"predictable training data shape: {df_train.shape}; others: {df_train_np.shape}")
            ai_logging.info("done predictable analysis.")
            if df_train.shape[0] == 0:
                ai_logging.info("No data to predict.")
                return None
        else:
            ai_logging.info("skip predictable analysis.")

        # 2.Clustering
        ai_model_dict = {}
        ai_logging.info("start generating group df.")
        df_train_list, df_val_list, key_list = gen_group_df(df_train, df_val,column_id, column_time, column_target, cluster_method)
        for i in range(len(key_list)):
            ai_model_dict['Group_{}'.format(i)] = {}
            ai_model_dict['Group_{}'.format(i)]['set_id'] = key_list[i].copy()
        ai_logging.info("done generating group df.")
        os.mkdir(report_path)
        tf = open("{}/group_set.json".format(report_path), "w")
        json.dump(ai_model_dict,tf)
        tf.close()
        
    else:
        ai_logging.info(f"===> Continue with Group_{start_group}.")
        ai_model_dict = json.loads(open(os.path.join(report_path, 'group_set.json')).read())
        df_train_list, df_val_list = [], []
        for k in ai_model_dict.keys():
            key_list = ai_model_dict[k]['set_id']
            df_train_list.append(df_train[df_train[column_id].isin(key_list)].drop(columns=[column_id, column_time]).reset_index(drop=True))
            df_val_list.append(df_val[df_val[column_id].isin(key_list)].drop(columns=[column_id, column_time]).reset_index(drop=True))
    
    # 3.Dimension reduction
    ai_pipe = AiPipeline(ai_pipeline_config) 
    for i in range(start_group, len(ai_model_dict.keys())):
        ai_logging.info(f"Group_{i} Start.")
        if ai_pipeline_config['DD_METHOD']:
            ai_logging.info("start dimension reduction.")
            train_data = df_train_list[i].drop([column_target], axis=1)
            train_data_y = df_train_list[i][column_target].copy()
            val_data = df_val_list[i].drop([column_target], axis=1)
            val_data_y = df_val_list[i][column_target].copy()
            dd_train_data,dd_val_data,dd_param = ai_pipe.gen_dd_data(
                train_data=train_data,train_data_y=train_data_y,\
                test_data=val_data,drop_original_feature=ai_pipeline_config['DROP_ORIGINAL_FEATURE'])
            ai_logging.info(f"Old data shape: {val_data.shape}; New data shape: {dd_val_data.shape}")
            x_train_list = [dd_train_data.reset_index(drop=True),train_data_y.reset_index(drop=True)]
            x_val_list = [dd_val_data.reset_index(drop=True),val_data_y.reset_index(drop=True)]
            report_group_path = '{}/Group_{}'.format(report_path,i)
            final_model_path = '{}/final_model'.format(report_group_path)
            if not os.path.exists(report_group_path):
                os.makedirs(report_group_path)
            if not os.path.exists(final_model_path):
                os.makedirs(final_model_path)
            save_dd_param(dd_param,final_model_path)
            ai_logging.info("done dimension reduction.")
        else:
            train_data = df_train_list[i].drop([column_target], axis=1)
            train_data_y = df_train_list[i][column_target].copy()
            val_data = df_val_list[i].drop([column_target], axis=1)
            val_data_y = df_val_list[i][column_target].copy()
            x_train_list = [train_data.reset_index(drop=True),train_data_y.reset_index(drop=True)]
            x_val_list = [val_data.reset_index(drop=True),val_data_y.reset_index(drop=True)]
            
        # 4. Building auto-model
        ai_logging.info(f"start auto-modeling.")
        candidate_list = []
        for method in ai_pip_estimator_list:
            ai_logging.info(f"running {method} ...")
            x_train,y_train = x_train_list
            x_val,y_val = x_val_list
            category_size = len(ai_model_dict['Group_{}'.format(i)]['set_id'])
            # if ai_model_dict['group_id'] == ['no group']:
            #     category_size = 1
            # else:
            #     category_size = len(key_list[i])
            report_group_path = '{}/Group_{}'.format(report_path,i)
            final_model_path = '{}/final_model'.format(report_group_path)
            best_model, best_params, best_score = ai_pipe.fit(x_train=x_train, \
                                                              y_train=y_train, \
                                                              x_val=x_val, \
                                                              y_val=y_val, \
                                                              method=method, \
                                                              target_directory=report_group_path, \
                                                              category_size=category_size)
            candidate_list.append({
                'method': method,
                'id_set': ai_model_dict['Group_{}'.format(i)]['set_id'].copy(),
                'best_model': best_model,
                'best_params': best_params,
                'best_score': best_score
            })
            ai_logging.info(f"done {method} modeling for group {i}: {ai_model_dict[f'Group_{i}']['set_id']}")
            # 刪除多餘的資料
        folder_clean(report_group_path, ai_pip_estimator_list)
        final_param,final_model = save_final_model(candidate_list=candidate_list,
                                                   final_model_path=final_model_path)
        ai_logging.info(f"Group_{i} End.")
    
    ai_logging.info("------------------done AiPipeline.------------------")
    return ai_model_dict, final_param, final_model
    
if __name__ == "__main__":
    main(df_train,df_val,report_path,column_time,column_target,ai_pipeline_config,column_id)
    print("Successfully calling main()")
