from AiPipeline import AiPipeline
import os
import pandas as pd
import numpy as np
from time import time
import json
import logging
from tensorflow.keras.models import load_model
import joblib
import math
from sklearn.metrics import *

configs = {
    "train_feature_path": "/home/b00175/TimeSeries/features/Taipower_data_raw_ft_31_202202_padding_train.csv",
    "validation_feature_path": "/home/b00175/TimeSeries/features/Taipower_data_raw_ft_31_202202_padding_val.csv",
    "test_feature_path": "/home/b00175/TimeSeries/features/Taipower_data_raw_ft_31_202202_padding_test.csv",
    "best_model_path": "report",
    'COLUMN_TIME': 'tx_dt',
    'COLUMN_TARGET': 'amount',
    'COLUMN_ID': 'category_id', #category_id,None
    "model_report_config": {
        "TRAIN_GROUP": "auto",
        "DROP_ORIGINAL_FEATURE": True,
        "RETRAIN_SIZE": 10,
        "TEST_SCORING": [
            'MAE', 'MAPE'#'EVS', 'MSE', 'R2', 'MSLE'
        ],
        "DD_METHOD_LIST": ["high_correlation","PCA","tsne"],
        "AI_PIP_TUNED_PARAMETERS": {
            'Auto_keras_tsf':{
                "tuner": 'bayesian', #random, grid, bayesian, greedy
                "lookback": 3,
                "max_trials": 1,
                "epochs": 1
            },
            'Auto_sk_tsf':{
                "time_left_for_this_task": 300,
                "per_run_time_limit": 300,
                "memory_limit": None,
                "ensemble_size": 10,
                "ensemble_nbest": 10,
                "n_jobs": 16,
                "exclude_preprocessors": {"feature_preprocessor": ["select_rates_regression"]},
                "delete_tmp_folder_after_terminate":False
            }
        },
        }
}

model_report_config = configs["model_report_config"]
column_time = configs['COLUMN_TIME']
column_target = configs['COLUMN_TARGET']
column_id = configs['COLUMN_ID']
best_model_path = configs['best_model_path']
# df_train = pd.read_csv(configs["train_feature_path"]).iloc[:500,:].drop(['category_id'], axis=1)
# df_val = pd.read_csv(configs["validation_feature_path"]).iloc[:200,:].drop(['category_id'], axis=1)
# df_test = pd.read_csv(configs["test_feature_path"]).iloc[:200,:].drop(['category_id'], axis=1)
df_train = pd.read_csv(configs["train_feature_path"])
df_train[column_id] = df_train[column_id].astype(str)
df_val = pd.read_csv(configs["validation_feature_path"])
df_val[column_id] = df_val[column_id].astype(str)
df_test = pd.read_csv(configs["test_feature_path"])
df_test[column_id] = df_test[column_id].astype(str)
np.random.seed(10)

def gen_predict_report(true_y, predict_y,metrics_list):
    metrics_dict = {
        'EVS' : explained_variance_score,
        'MAE' : mean_absolute_error,
        'MSE' : mean_squared_error,
        'MSLE' : mean_squared_log_error,
        'MAPE' : mean_absolute_percentage_error,
        'R2' : r2_score,

    }
    result_dict = {}
    for metrics in metrics_list:
        result_dict[metrics] = metrics_dict[metrics](true_y, predict_y)#
    return result_dict

def load_final_model(model_path):
    with open('{}/final_param.json'.format(model_path), newline='') as jsonfile:
        final_param = json.load(jsonfile)
    method = final_param['method']
    if method=='Auto_keras_tsf':
        model = load_model('{}/final_model.h5'.format(model_path))
    elif method=='Auto_sk_tsf':
        model = joblib.load('{}/final_model.pkl'.format(model_path))
    return model,method

def load_dd_param(model_path,config):
    dd_param = {}
    if 'high_correlation' in config:
        with open('{}/high_correlation.json'.format(model_path), newline='') as jsonfile:
            dd_param['high_correlation'] = json.load(jsonfile)
    if 'LDA' in config:
        dd_param['LDA'] = joblib.load('{}/LDA.pkl'.format(model_path))
    if 'PCA' in config:
        dd_param['PCA'] = joblib.load('{}/PCA.pkl'.format(model_path))
    if 'tsne' in config:
        dd_param['tsne'] = joblib.load('{}/tsne.pkl'.format(model_path))
    return dd_param

def load_group_set(group_set_path):
    with open('{}/group_set.json'.format(group_set_path), newline='') as jsonfile:
        group_set_dict = json.load(jsonfile)
    return group_set_dict

def gen_group_df(data,column_id,key_list):
    group_df = data.loc[[data[column_id] in key] ,:].drop([column_id], axis=1)
    return group_df.copy()

def main(df_train,df_val,df_test,column_time,column_target,best_model_path,model_report_config,column_id=None):
    logging.basicConfig(filename="model_report.log", level=logging.INFO, filemode= 'w', \
                        format="%(levelname)s:%(asctime)s:%(message)s", datefmt='%m/%d/%Y %I:%M:%S %p')

    df_train_val = pd.concat([df_train, df_val], ignore_index=True).sort_values(column_time)
    df_test = df_test.sort_values(column_time)
    group_set_dict = load_group_set(group_set_path=best_model_path)
    result_dict = {}
    logging.info(f"start data filtering.")
    for group_key in group_set_dict.keys():
        group_set = group_set_dict[group_key]['set_id'].copy()
        model,method = load_final_model(model_path="{}/{}/final_model".format(best_model_path,group_key))

        if group_set != ['no_group']:
            sub_df_train_val = df_train_val.loc[[i in group_set for i in df_train_val[column_id]] ,:].drop([column_id,column_time], axis=1).reset_index(drop=True)
            sub_df_test = df_test.loc[[i in group_set for i in df_test[column_id]] ,:].drop([column_id,column_time], axis=1).reset_index(drop=True)
        else:
            sub_df_train_val = df_train_val.copy().reset_index(drop=True)
            sub_df_test = df_test.copy().reset_index(drop=True)

        
        retrain_size = model_report_config['RETRAIN_SIZE']
        id_number = len(group_set)
        test_part = retrain_size*id_number
        max_return_time = math.ceil(len(sub_df_test)/test_part)
        predict_Y = []
        for return_time in range(1,max_return_time+1):
            test_data = sub_df_test.loc[test_part*(return_time-1):test_part*return_time-1,:].drop([column_target], axis=1)
            train_data = pd.concat([
                sub_df_train_val,
                sub_df_test.loc[:test_part*(return_time-1),:]
            ]).drop([column_target], axis=1)
            train_data_y = pd.concat([
                sub_df_train_val,
                sub_df_test.loc[:test_part*(return_time-1),:]
            ])[column_target]
            logging.info(f"start dimension reduction transformation {return_time} .")
            ai_pipe = AiPipeline(model_report_config)
            if model_report_config['DD_METHOD_LIST']:
                dd_param = load_dd_param(model_path="{}/{}/final_model".format(best_model_path,group_key),
                                         config=model_report_config['DD_METHOD_LIST'])
                dd_train_data = ai_pipe.gen_dd_data_transform(data=train_data,
                                                              dd_param=dd_param,
                                                              drop_original_feature=model_report_config['DROP_ORIGINAL_FEATURE'])
                dd_test_data = ai_pipe.gen_dd_data_transform(data=test_data,
                                                              dd_param=dd_param,
                                                              drop_original_feature=model_report_config['DROP_ORIGINAL_FEATURE'])
                x_train = [dd_train_data.reset_index(drop=True),train_data_y.reset_index(drop=True)]
                x_test = [dd_test_data.reset_index(drop=True)]
            else:
                x_train = [train_data.reset_index(drop=True),train_data_y.reset_index(drop=True)]
                x_test = [test_data.reset_index(drop=True)]
            logging.info(f"start refitting {return_time} .")
            refit_model = ai_pipe.refit(method=method, \
                                        model=model, \
                                        x=x_train[0], \
                                        y=x_train[1])
            logging.info(f"start predicting {return_time} .")
            predict_y = ai_pipe.predict(method=method, \
                                        model=model, \
                                        x=pd.concat([x_train[0], x_test[0]], ignore_index=True), \
                                        predict_length=len(x_test[0]))
            predict_Y += predict_y

        result_dict[group_key] = gen_predict_report(
            true_y = list(sub_df_test[column_target].values), 
            predict_y=predict_Y,
            metrics_list=model_report_config['TEST_SCORING'])
            
    tf = open("{}/test_result.json".format(best_model_path), "w")
    json.dump(result_dict, tf)
    tf.close()
    return result_dict
 
    
if __name__ == "__main__":
    main(df_train,df_val,df_test,column_time,column_target,best_model_path,model_report_config,column_id)
    print("Successfully calling main()")
