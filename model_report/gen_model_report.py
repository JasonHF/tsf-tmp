import pandas as pd
import numpy as np

def rolling_predict(train_data,train_data_y,test_data,test_data_y,method,best_params,retrain_size):
    if len(train_data)!=len(train_data_y):
        raise BaseException("train_data lenght {} != train_data_y lenght {}".format(len(train_data),len(train_data_y)))
    if len(test_data)!=len(test_data_y):
        raise BaseException("test_data lenght {} != test_data_y lenght {}".format(len(test_data),len(test_data_y)))
    train_X = train_data.values
    train_y = train_data_y.values
    test_X = test_data.values
    test_y = test_data_y.values
    retrain_times = int(np.ceil(len(test_data_y)/retrain_size))
    predict_y_list = []
    for i in range(retrain_times):
        if i==0:
            retrain_train_X = train_X.copy()
            retrain_train_y = train_y.copy()
            predict_X = test_X[:retrain_size].copy()
        else :
            retrain_train_X = np.concatenate((train_X,test_X[retrain_size*(i-1):retrain_size*i]),axis=0)
            retrain_train_y = np.concatenate((train_y,test_y[retrain_size*(i-1):retrain_size*i]),axis=0)
            if (i==retrain_times-1) and (len(test_data_y)%retrain_size!=0):
                predict_X = test_X[retrain_size*i:].copy()
            else:
                predict_X = test_X[retrain_size*(i-1):retrain_size*i].copy()
                
        model = method(**best_params)
        model.fit(retrain_train_X, retrain_train_y) 
        predict_y = model.predict(predict_X)
        predict_y_list += list(predict_y)
    if len(predict_y_list) != len(test_data_y):
        raise BaseException("predict_y_list lenght {} != test_data_y lenght {}".format(len(predict_y_list),len(test_data_y)))
    return predict_y_list

def gen_predict_report(ture_y, predict_y,method):
    return method(ture_y, predict_y)
