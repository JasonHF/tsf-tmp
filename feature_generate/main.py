import feature_generator
import os
import pandas as pd
from time import time
import json
import logging

configs =  {
    "train_data_path": "/home/b00175/TimeSeries/data/Taipower_data_raw_31_202202_padding_train.csv",
    "validation_data_path": "/home/b00175/TimeSeries/data/Taipower_data_raw_31_202202_padding_val.csv",
    "test_data_path": "/home/b00175/TimeSeries/data/Taipower_data_raw_31_202202_padding_test.csv",
    "feature_generate_config": {
        "COLUMN_ID": "category_id",
        "COLUMN_TIME": "tx_dt",
        "COLUMN_TARGET": "amount",
        "FG_WINDOW_SIZE_LIST": [
            12, 
            18
        ],
        "TAT_USE_CATEGORY": [
            "Complexity", 
            "CycleIndicators", 
            "LogicalCondition", 
            "MathFunctions", 
            "MomentumIndicators", 
            "MovingAverage", 
            "StatisticFeatures", 
            "StatisticModels"
        ],
        "FN_METHOD": "minmax"
    }
}
df_train = pd.read_csv(configs["train_data_path"], dtype = {configs["feature_generate_config"]["COLUMN_ID"]:str})
df_val = pd.read_csv(configs["validation_data_path"], dtype = {configs["feature_generate_config"]["COLUMN_ID"]:str})
df_test = pd.read_csv(configs["test_data_path"], dtype = {configs["feature_generate_config"]["COLUMN_ID"]:str})
        
FG = feature_generator.FeatureGenerator(configs["feature_generate_config"])

def main():
    logging.basicConfig(filename="FeatureGenerator.log", level=logging.INFO, filemode= 'w', format="%(levelname)s:%(asctime)s:%(message)s", datefmt='%m/%d/%Y %I:%M:%S %p')
    start = time()
    ft_train, ft_val, ft_test = FG.generate(df_train, df_val, df_test)
    logging.info(f"It took {(time() - start)//60} minutes")
    return ft_train, ft_val, ft_test
    
if __name__ == "__main__":
    ft_train, ft_val, ft_test = main()
    ft_train.to_csv("/home/b00175/TimeSeries/features/Taipower_data_raw_ft_31_202202_padding_train.csv", index = False)
    ft_val.to_csv("/home/b00175/TimeSeries/features/Taipower_data_raw_ft_31_202202_padding_val.csv", index = False)
    ft_test.to_csv("/home/b00175/TimeSeries/features/Taipower_data_raw_ft_31_202202_padding_test.csv", index = False)
    print("Successfully calling main()")
