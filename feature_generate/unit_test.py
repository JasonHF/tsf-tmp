import unittest
from utils import check_config, get_default_config, set_config
from utils import verify_dateframes_frequency, verify_time_complete
from utils import data_combine, data_split
from utils import generate_features
from utils import fill_notfinite
from utils import feature_normalize
import logging
import os
import pandas as pd
import numpy as np

CONFIG = {
            "train_data_path": "data/train1.csv",
            "validation_data_path": "data/validation1.csv",
            "test_data_path": "data/test1.csv",
            "feature_generate_config": {
                "COLUMN_ID": "category_id",
                "COLUMN_TIME": "tx_dt",
                "COLUMN_TARGET": "amount",
                "FG_WINDOW_SIZE_LIST": [
                    10, 
                    20,
                ],
                "TAT_USE_CATEGORY": [
                    "Complexity", 
                    "CycleIndicators", 
                    "LogicalCondition", 
                    "MathFunctions", 
                    "MomentumIndicators", 
                    "MovingAverage", 
                    "StatisticFeatures", 
                    "StatisticModels"
                ],
                "FN_METHOD": "minmax"
            }
        }
DATAFRAME = pd.read_csv("unit_test_data/fake_data.csv")
DATAFRAME2 = pd.read_csv("unit_test_data/fake_data_2.csv")
DATAFRAME3 = pd.read_csv("unit_test_data/fake_data_3.csv")

class TestMethods(unittest.TestCase):
    def test_generate_features(self, df = DATAFRAME2):
        error_msg = check_config(CONFIG)
        config = set_config(CONFIG, get_default_config())
        feature_data = generate_features(df=df, column_id="category_code", column_time="tx_dt", column_target='amount', config=config['feature_generate_config'])
        self.assertTrue(len(feature_data.columns)==2248)
        self.assertTrue(len(feature_data)==len(df)-19*2)
        
    def test_fill_notfinite(self, df = DATAFRAME3):
        fill_data = fill_notfinite(data=df,date_col="tx_dt",group_columns=['category_code'],method='ffill',min_data_len_rate=0.3)
        ans_fill_data = pd.read_csv('unit_test_data/ans_fill_data.csv')
        self.assertFalse('feature3' in ans_fill_data.columns)
        self.assertTrue((fill_data.feature1.values.astype('float') == ans_fill_data.feature1.values.astype('float')).all())
        self.assertTrue((fill_data.feature2.values.astype('float') == ans_fill_data.feature2.values.astype('float')).all())
        
    def test_normalize(self, df = DATAFRAME):
        # sub-test1
        minmax_normal = feature_normalize(df = df, column_id = "category_code", column_time = "tx_dt", 
                                      column_filter_list = [], window_length = 0, shift = 0, method = "minmax")
        auto_answer = np.array(minmax_normal['amount']).round(6).tolist()
        hand_writed_answer = np.hstack((np.zeros(30),np.linspace(0,1,30),np.linspace(0,1,10),np.linspace(0,1,10),np.linspace(0,1,10)))
        hand_writed_answer = hand_writed_answer.round(6).tolist()
        self.assertEqual(auto_answer, hand_writed_answer)
        
        # sub-test2
        standard_normal = feature_normalize(df = df, column_id = "category_code", column_time = "tx_dt", 
                                      column_filter_list = [], window_length = 0, shift = 0, method = "standard")
        auto_answer = np.array(standard_normal['amount']).round(6).tolist()
        tmp = (np.array(range(1,11)) - 5.5) / (2.921384)
        hand_writed_answer = np.hstack((np.zeros(30),
                                        (np.array(range(1,31)) - 15.5) / (77.5**(0.5)),
                                        tmp,tmp,tmp))
        hand_writed_answer = hand_writed_answer.round(6).tolist()            
        self.assertEqual(auto_answer, hand_writed_answer)
        
        # sub-test3
        minmax_win10 = feature_normalize(df = df, column_id = "category_code", column_time = "tx_dt", 
                                      column_filter_list = [], window_length = 10, shift = 0, method = "minmax")
        auto_answer = np.array(minmax_win10['amount']).round(6).tolist()
        hand_writed_answer = np.hstack((np.zeros(30),
                                       np.zeros(1), np.ones(29),
                                       np.zeros(1), np.ones(9), np.linspace(0,10,10)/10, np.linspace(0,10,10)/10))
        hand_writed_answer = hand_writed_answer.round(6).tolist()
        self.assertEqual(auto_answer, hand_writed_answer)
        
        # sub-test4
        minmax_win10_shift1 = feature_normalize(df = df, column_id = "category_code", column_time = "tx_dt", 
                              column_filter_list = [], window_length = 10, shift = 1, method = "minmax")
        
        auto_answer = np.array(minmax_win10_shift1['amount']).round(6).tolist()
        # print(auto_answer)
        hand_writed_answer = np.array([0]*30 + 
                                      [0,1,2.0,1.5,1.333333333,1.25,1.2,1.166666667,1.142857143,1.125,1.111111111,1.111111111,
                                               1.111111111,1.111111111,1.111111111,1.111111111,1.111111111,1.111111111,1.111111111,
                                               1.111111111,1.111111111,1.111111111,1.111111111,1.111111111,1.111111111,1.111111111,
                                               1.111111111,1.111111111,1.111111111,1.111111111] + 
                                      [0, 1, 2.0, 1.5, 1.3333333333333333, 1.25, 1.2, 1.1666666666666667, 1.1428571428571428, 1.125, 0.0,
                                       0.1111111111111111, 0.2222222222222222, 0.3333333333333333, 0.4444444444444444, 0.5555555555555556,
                                       0.6666666666666666, 0.7777777777777778, 0.8888888888888888, 1.0, 0.0, 0.1111111111111111,
                                       0.2222222222222222, 0.3333333333333333, 0.4444444444444444, 0.5555555555555556, 0.6666666666666666,
                                       0.7777777777777778, 0.8888888888888888, 1.0])
        hand_writed_answer = hand_writed_answer.round(6).tolist()
        # print(hand_writed_answer)
        self.assertEqual(auto_answer, hand_writed_answer)

    def test_time_frequency(self, df = DATAFRAME):
        df_w1 = DATAFRAME[DATAFRAME["category_code"] == "W100"]
        df_w2 = DATAFRAME[DATAFRAME["category_code"] == "W200"]
        df_w3 = DATAFRAME[DATAFRAME["category_code"] == "W300"]
        
        # sub-test 1
        auto_answer = verify_dateframes_frequency([df_w1], ["tx_dt"])
        hand_writed_answer = True
        self.assertEqual(auto_answer, hand_writed_answer)
        
        # sub-test 2
        auto_answer = verify_dateframes_frequency([df_w2], ["tx_dt"])
        hand_writed_answer = True
        self.assertEqual(auto_answer, hand_writed_answer)

        # sub-test 3
        auto_answer = verify_dateframes_frequency([df_w3], ["tx_dt"])
        hand_writed_answer = True
        self.assertEqual(auto_answer, hand_writed_answer)

        # sub-test 4
        auto_answer = verify_dateframes_frequency([df_w1,df_w2], ["tx_dt","tx_dt"])
        hand_writed_answer = True
        self.assertEqual(auto_answer, hand_writed_answer) 
        
        # sub-test 5
        auto_answer = verify_dateframes_frequency([df_w1,df_w2,df_w3], ["tx_dt","tx_dt","tx_dt"])
        hand_writed_answer = False
        self.assertEqual(auto_answer, hand_writed_answer)        
        
    def test_time_complete(self, df = DATAFRAME):
        df_w1 = DATAFRAME[DATAFRAME["category_code"] == "W100"]
        df_w2 = DATAFRAME[DATAFRAME["category_code"] == "W200"]
        df_w3 = DATAFRAME[DATAFRAME["category_code"] == "W300"]
        
        # sub-test 1
        auto_answer = verify_time_complete(df_w1, "category_code", "tx_dt")
        hand_writed_answer = True
        self.assertEqual(auto_answer, hand_writed_answer)
        
        # sub-test 2
        self.assertRaises(BaseException, verify_time_complete, df_w2, "category_code", "tx_dt")
        
        # sub-test 3
        auto_answer = verify_time_complete(df_w3, "category_code", "tx_dt")
        hand_writed_answer = True
        self.assertEqual(auto_answer, hand_writed_answer)
        
        # sub-test 4
        self.assertRaises(BaseException, verify_time_complete, df, "category_code", "tx_dt")

if __name__ == '__main__':
    unittest.main()