from utils import check_config, get_default_config, set_config
from utils import verify_dateframes_frequency, verify_time_complete
from utils import data_combine, data_split
from utils import generate_features
from utils import fill_notfinite
from utils import feature_normalize
import logging
import os

class FeatureGenerator:
    
    def __init__(self, config):
        self._error_msg = check_config(config)
        self.config = set_config(config, get_default_config())
        self._df_all, self._df_reserved, self._end_datetime_list = None, None, None
        self.column_id, self.column_time, self.column_target = None, None, None
    
        for msg in self._error_msg:
            logging.error(msg)
        if self._error_msg != []:
            raise BaseException("Function FeatureGenerator.check_config Failed.")

    def _combine(self, train, val, test):
        df_list = [train, val, test]
        self.column_id = self.config['COLUMN_ID'] #"category_code"
        self.column_time = self.config['COLUMN_TIME'] #"tx_dt"
        column_time_list = [self.column_time] * len(df_list)
        self.column_target = self.config['COLUMN_TARGET'] #"week_qty"
        drop_list = [ col for col in train.columns.tolist() if col not in [self.column_id, self.column_time, self.column_target]]
        if verify_dateframes_frequency(df_list,column_time_list):
            self._df_all, self._df_reserved, self._end_datetime_list = data_combine(df_list, self.column_id, self.column_time, drop_list)
            self._df_all = self._df_all.reset_index(drop=True)
            self._df_reserved = self._df_reserved.reset_index(drop=True)
            
            if not verify_time_complete(self._df_all, self.column_id, self.column_time):
                logging.error("Function FeatureGenerator._combine.verity_time_complete Failed.")
                self._error_msg.append("Function FeatureGenerator._combine.verity_time_complete Failed.")
                return False
            else:
                return True
        else:
            logging.error("Function FeatureGenerator._combine.verify_dateframes_frequency Failed.")
            self._error_msg.append("Function FeatureGenerator._combine.verify_dateframes_frequency Failed.")
            return False


    def generate(self, train, val, test):
        is_combine = self._combine(train, val, test)
        if is_combine:
            logging.info("combine successful.")
            self._df_all = generate_features(self._df_all, self.column_id, self.column_time, self.column_target, self.config)
            logging.info("generate_features successful.")
            self._df_all = fill_notfinite(self._df_all, self.column_time, self.column_target, [self.column_id],  self.config['FNF_METHOD'],  self.config['FNF_MIN_DATA_LENGTH'])
            logging.info("fill_notfinite successful.")
            self._df_all = feature_normalize(self._df_all, self.column_id, self.column_time, [self.column_target],  self.config['FN_WINDOW_SIZE'],  self.config['FN_SHIFT_SIZE'],  self.config['FN_METHOD'])
            logging.info("feature_normalize successful")
            ft_train, ft_val, ft_test = data_split(self._df_all, self.column_id, self.column_time, self._end_datetime_list, self._df_reserved)
            logging.info("data_split successful")
            return ft_train, ft_val, ft_test
        else:
            logging.error("Function FeatureGenerator.generate Failed.")
            self._error_msg.append("Function FeatureGenerator.generate Failed.")
            raise BaseException("Function FeatureGenerator.generate Failed.")

