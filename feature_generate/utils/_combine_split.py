import pandas as pd
def data_combine(df_list, column_id, column_time, column_filter_list):
    """
    df_list:list of pd.DataFrame
    column_id:str= the column name of category
    column_time:str= the column name of datetime
    column_filter_list = the list of column names which are not used in feature generation
    """
    df_combined = pd.DataFrame()
    end_datetime_list = []
    for df_tmp in df_list:
        df_tmp = df_tmp.sort_values(by = [column_id, column_time], axis = 0)
        end_datetime_list.append(df_tmp[column_time].values[-1])
        df_combined = pd.concat([df_combined, df_tmp])
    
    df_combined = df_combined.sort_values(by = [column_id, column_time], axis = 0)
    df_dropped = df_combined[column_filter_list]
    df_dropped.reset_index(drop = True, inplace = True)
    
    df_combined = df_combined.drop(columns = column_filter_list)
    df_combined.reset_index(drop = True, inplace = True)
    
    return df_combined, df_dropped, end_datetime_list

def data_split(df_feature, column_id, column_time, end_datetime_list, df_reserved):
    """
    df_feature:pd.DataFrame
    column_id:str= the column name of category
    column_time:str= the column name of datetime
    end_datatime_list:list of datetime =  the list of last datetime for each dataframe
    df_reserved:pd.DataFrame = df dropped in function:data_combine to re-join with feature dataframe
    """
    df_feature = df_feature.sort_values(by = [column_id, column_time], axis = 0)
    df_feature = pd.concat([df_feature, df_reserved], axis = 1)
    df_feature.reset_index(drop = True, inplace = True)
    
    df_split_list = []
    for end_datetime in end_datetime_list:
        df_split = df_feature[df_feature[column_time]<=str(end_datetime)]
        df_split_list.append(df_split)
        df_feature = df_feature[df_feature[column_time]>str(end_datetime)]
        
    return df_split_list