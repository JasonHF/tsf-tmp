# coding: utf-8

import pandas as pd
import datetime
import string
import logging


def _verify_value_datetime(datetime_value):
    """
    1. 判斷型態是否為datetime，是就True
    2. 若為str則試試看轉成datetime
    3. 若str的格式不符合ISO8601則回傳False
    
    規定: 輸入日期必須為datetime型態，或至少符合ISO8601格式('%Y-%m-%dT%H:%M:%S')的string。
    """
    ISO8601_FORMAT1 = '%Y-%m-%dT%H:%M:%S'
    ISO8601_FORMAT2 = '%Y-%m-%d'
    str_format_list = [ISO8601_FORMAT1, ISO8601_FORMAT2]
    checked = False
    if type(datetime_value) == datetime.datetime:
        checked = True
        
    elif type(datetime_value) == str:
        for str_format in str_format_list:
            try:
                datetime_value = datetime.datetime.strptime(datetime_value, str_format)
            except:
                pass
        if type(datetime_value) == datetime.datetime:
            checked = True
    if checked == True:
        return datetime_value
    else:
        return ValueError("Only datetime and ISO8601 string format are acceptable.")

def _verify_dataframe_datetime(df, column_time):
    """
    判斷整個欄位是否都有符合format，是就返回True，否就raise error
    Input:
    df:pd.DataFrame
    column_time:str = name of datetime column in df. 
    
    Output:
    True or ValueError
    """
    
    check_list = list(df[[column_time]].applymap(_verify_value_datetime).iloc[:,0])
    checked = all(check_list)
    
    if checked == False:
        raise ValueError(f"Unsupported type in column: {column_time} ")
        
    return checked

def _get_minimal_unit(datetime_value):
    
    hour, minute, second = datetime_value.hour, datetime_value.minute, datetime_value.second
    if hour == minute == second == 0:
        minimal_unit = "day"
    else:
        minimal_unit = "sec"
    return minimal_unit

def verify_dateframes_frequency(df_list, column_time_list):
    """
    判斷多個表格的時間欄位是否有相同的最小單位，是就返回True，否則返回False
    Input:
    df_list:list of pd.DataFrame
    column_time_list:str = list of name of datetime column order by df_list
    
    Output:
    True or False
    """
    # 檢查長度是否相同，不相同就噴error
    if len(df_list) != len(column_time_list):
        raise "length of df_list must equal to length of column_time_list "
    
    # 檢查整個df是否符合format，不符合就噴error
    for i, df in enumerate(df_list):
        column_time = column_time_list[i]
        _verify_dataframe_datetime(df, column_time)
    
    standard_datetime_unit_list = []
    # 檢查單一df內是否同長度
    for i, df in enumerate(df_list):
        column_time = column_time_list[i]
        time_list = list(df[[column_time]].applymap(_verify_value_datetime).iloc[:,0])
        
        # 取首個欄位值作為模範(ISO8601必有年月日，故最小單位只有可能比day還小)
        first_dt = time_list[0]
        minimal_unit = _get_minimal_unit(first_dt)

        # 檢查是否整個欄位都和模範有一樣的最小單位
        inner_checked = all([_get_minimal_unit(t) == minimal_unit for t in time_list])
        if inner_checked == True:
            standard_datetime_unit_list.append(minimal_unit)
        else:
            raise BaseException(f"The datetime frequency in #{i+1} dataframe are not consistent.")
            
    # 檢查多個df是否同長度
    standard_datetime_unit = standard_datetime_unit_list[0]
    outer_checked = all([l == standard_datetime_unit for l in standard_datetime_unit_list])
    if outer_checked == True:
        return outer_checked
    else:
        print(f"minimal unit of datetime in order of df_list: {standard_datetime_unit_list}")
        return outer_checked
        

def verify_time_complete(df, column_id, column_time):
    """
    判斷時間欄位是否為等差數列
    time_series: pandas.core.series.Series
    """
    df = df.copy()
    check = True
    df[column_time] = df[column_time].apply(_verify_value_datetime)
    id_series = list(set(df[column_id]))
    
    # 年資料
    if all(df[column_time].apply(lambda _: _.month) == 1) and all(df[column_time].apply(lambda _: _.day) == 1):
        logging.info("yearly data")
        print("yearly data")
        df[column_time] = df[column_time].apply(lambda _: _.year)
        for category in id_series:
            df_tmp = df[df[column_id] == category]
            df_tmp = df_tmp.sort_values(by = [column_time], axis = 0)
            df_tmp.reset_index(drop = True, inplace = True)

            number_datetime = df_tmp.shape[0]
            if number_datetime > 2:
                base_timedelta = df_tmp[column_time][1] - df_tmp[column_time][0]
                for irow in range(1, number_datetime):
                    tmp_timedelta = df_tmp[column_time][irow] - df_tmp[column_time][irow-1]
                    if tmp_timedelta != base_timedelta:
                        check = False
                        raise BaseException(f"The timedelta between row {irow} and row {irow-1} are not equal to '{base_timedelta}' in {category}.")
    # 月資料
    elif all(df[column_time].apply(lambda _: _.day) == 1):
        logging.info("monthly data")
        print("monthly data")
        df[column_time] = df[column_time].apply(lambda _: str(_.year) + str(_.month) if _.month > 9 else str(_.year) + "0" + str(_.month))
        df[column_time] = df[column_time].apply(lambda _: datetime.datetime.strptime(_,"%Y%m"))
        for category in id_series:
            df_tmp = df[df[column_id] == category]
            df_tmp = df_tmp.sort_values(by = [column_time], axis = 0)
            df_tmp.reset_index(drop = True, inplace = True)

            number_datetime = df_tmp.shape[0]
            if number_datetime > 2:
                base_timedelta = 12*(df_tmp[column_time][1].year - df_tmp[column_time][0].year) + df_tmp[column_time][1].month - df_tmp[column_time][0].month
                for irow in range(1, number_datetime):
                    tmp_timedelta = 12*(df_tmp[column_time][irow].year - df_tmp[column_time][irow-1].year) + df_tmp[column_time][irow].month - df_tmp[column_time][irow-1].month
                    if tmp_timedelta != base_timedelta:
                        check = False
                        raise BaseException(f"The timedelta between row {irow} and row {irow-1} are not equal to '{base_timedelta}' in {category}.")    
    # 日資料以下
    else:
        logging.info("daily data or more freq.")
        print("daily data")
        for category in id_series:
            df_tmp = df[df[column_id] == category]
            df_tmp = df_tmp.sort_values(by = [column_time], axis = 0)
            df_tmp.reset_index(drop = True, inplace = True)

            number_datetime = df_tmp.shape[0]
            if number_datetime > 2:
                base_timedelta = pd.Timedelta(df_tmp[column_time][1] - df_tmp[column_time][0], unit = "sec")
                for irow in range(1, number_datetime):
                    tmp_timedelta = pd.Timedelta(df_tmp[column_time][irow] - df_tmp[column_time][irow-1], unit = "sec")
                    if tmp_timedelta != base_timedelta:
                        check = False
                        raise BaseException(f"The timedelta between row {irow} and row {irow-1} are not equal to '{base_timedelta}' in {category}.")
    return check