import pandas as pd
import numpy as np
import logging
import sys

def gen_fill_feature_list(data,min_data_len_rate=0.7):
    data_describe = data.describe().T.copy()
    nofill_feature_list = data_describe.loc[(data_describe['count']==len(data))&\
                                            (data_describe['min'].notnull())&\
                                            (data_describe['max'].notnull())].index.tolist()
    fill_feature_list = data_describe.loc[(data_describe['count']<len(data))&\
                                          (data_describe['count']>min_data_len_rate*len(data))&\
                                          (data_describe['min'].notnull())&\
                                          (data_describe['max'].notnull())].index.tolist()
    much_na_columns = data_describe.loc[(data_describe['count']<=min_data_len_rate*len(data))].index.tolist()
    return nofill_feature_list, fill_feature_list, much_na_columns

def fill_notfinite(data,date_col,x_col,group_columns=None,method='ffill',min_data_len_rate=0.7):
    
    logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)
    
    fill_data = data.copy()
    fill_data.replace([np.inf, -np.inf], np.nan, inplace=True)
    no_rocket_feature_list = list(fill_data.columns[[(i.find('rocket_feature')==-1) and \
                                                     (i.find(x_col)==-1) and \
                                                     (i.find(date_col)==-1) for i in fill_data.columns]])
    rocket_feature_list = list(fill_data.columns[[i.find('rocket_feature')>=0 for i in fill_data.columns]])
    if group_columns:
        for i in group_columns:
            no_rocket_feature_list.remove(i)
        Group_df_list = []
        fill_feature_list_union = []
        much_na_columns_union = []
        for key, group_df in fill_data.groupby(group_columns):
            nofill_feature_list, fill_feature_list, much_na_columns = \
                gen_fill_feature_list(group_df.loc[:,no_rocket_feature_list], min_data_len_rate)
            nofill_feature_list += rocket_feature_list
            group_df = group_df.loc[:,group_columns + [date_col] + [x_col] + \
                                    nofill_feature_list + fill_feature_list]
            group_df[fill_feature_list] = group_df.groupby(group_columns)[fill_feature_list]\
            .transform(lambda x: x.fillna(method=method))
            Group_df_list.append(group_df.dropna())
            fill_feature_list_union = list(set(fill_feature_list_union) | set(fill_feature_list))
            much_na_columns_union = list(set(much_na_columns_union) | set(much_na_columns))
#   
        Group_df = pd.concat(Group_df_list)
        logging.info('fill_feature_list_union {}'.format(len(fill_feature_list_union)))
        logging.info(fill_feature_list_union)
        logging.info('much_na_columns_union {}'.format(len(much_na_columns_union)))
        logging.info(much_na_columns_union)
        Group_df_dropna = Group_df.dropna(axis = 1)
        if len(Group_df_dropna.columns) != len(data.columns)-len(set(much_na_columns_union)):
            raise BaseException("""filled columns length is {} not match \n
                                 must be {}-{}={}""".format(len(Group_df_dropna.columns),len(data.columns),\
                                                               len(set(much_na_columns_union)),\
                                                               len(data.columns)-len(set(much_na_columns_union))))
        return Group_df_dropna
    else:
        nofill_feature_list, fill_feature_list, much_na_columns = \
            gen_fill_feature_list(fill_data.loc[:,no_rocket_feature_list])
        nofill_feature_list += rocket_feature_list
        logging.info('fill_feature_list {}'.format(len(fill_feature_list)))
        logging.info(fill_feature_list)
        logging.info('much_na_columns {}'.format(len(much_na_columns)))
        logging.info(much_na_columns)
        fill_data = fill_data.loc[:,[date_col] + [x_col] + nofill_feature_list + fill_feature_list]
        fill_data[fill_feature_list] = fill_data[fill_feature_list]\
        .transform(lambda x: x.fillna(method=method))
        fill_data = fill_data.dropna()
        if len(fill_data.columns) != len(data.columns)-len(much_na_columns):
            raise BaseException("""filled columns length is {} not match \n
                                 must be {}-{}={}""".format(len(fill_data.columns),len(data.columns),\
                                                               len(much_na_columns),\
                                                               len(data.columns)-len(much_na_columns)))
        return fill_data