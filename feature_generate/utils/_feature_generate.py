# -*- coding: utf-8 -*-
from datetime import datetime
import logging
import numpy as np
import pandas as pd
from sktime.transformations.panel.rocket import MiniRocket
from sktime.transformations.panel.rocket import Rocket
import talib as ta
from tsfresh.feature_extraction import extract_features
from tsfresh.utilities.dataframe_functions import roll_time_series

def generate_features(df, column_id, column_time, column_target, config, verbose=False):
    config = _filter_features(config)
    is_mini = config['RK_USED_MINI']
    random_state = config['RK_RANDOM_STATE']
    rocket_n_features = config['RK_FEATURES']
    window_length = config['FG_WINDOW_SIZE_LIST']
    
    all_features = df
    rocket_df = None
    tsfresh_df = None
    
    for win_len in window_length:
        all_features = _talib_feature(all_features, column_id, column_time, column_target, win_len, config['TALIB_CONFIG'])
        
        logging.info(f"generate_features win_size:{win_len} talib successful")
        
        rolled_df = roll_time_series(df, column_id=column_id, column_sort=column_time, 
                                    max_timeshift=win_len-1, min_timeshift=win_len-1, disable_progressbar=not verbose)
        
        tsfresh_df = _tsfresh_features(rolled_df, column_id, column_time, column_target, win_len, config['TSFRESH_CONFIG'])
        logging.info(f"generate_features win_size:{win_len} tsfresh successful")
        all_features = all_features.merge(tsfresh_df ,how='inner', on=[column_id, column_time])
    
    df = df.sort_values(by = [column_id, column_time], axis = 0) #對種類和時間做排序
    timeshift = max(window_length) - 1 # 窗格長度
    num_category = df.groupby(column_id).count().shape[0] #種類數量
    num_windows = df.groupby(column_time).count().shape[0] - timeshift #窗格數量
    category_code = df[column_id].unique().tolist() # 所有種類
    datetime_code = df[column_time].unique().tolist() # 所有時間點
    rocket_df = _rocket_features(rolled_df, column_id, column_time, column_target, timeshift, rocket_n_features, num_category, num_windows, \
                                 category_code, datetime_code, is_mini, random_state)
    if not rocket_df is None:
            all_features = pd.concat([all_features,rocket_df], axis = 1)
    logging.info('generate_features rocket successful')
    
    rename_function_names = _rename_functions(all_features.columns.tolist())
    all_features.rename(columns=rename_function_names, inplace=True)
    
    return all_features


def _rocket_features(rolled_df, column_id, column_time, column_target, timeshift, rocket_n_features, num_category, num_windows, \
                                         category_code, datetime_code, is_mini, random_state):
    icol_timeseries = rolled_df.columns.get_loc(column_target) # 時間序列所在的欄位index
    window_length = timeshift + 1
    X = pd.DataFrame()
    rownames_list = [f"roll_window_{iw}" for iw in range(num_windows)]
    for icol, code in enumerate(category_code):
        rolled_df_tmp = rolled_df[rolled_df[column_id]==code]
        timeseries_list = [pd.Series(rolled_df_tmp.iloc[window_length * iw : window_length * (iw + 1), icol_timeseries]).reset_index(drop=True) for iw in range(num_windows)]
        X_tmp = pd.DataFrame({code:timeseries_list}, index = rownames_list).transpose() # nested dataframe
        X = X.append(X_tmp) #將種類合併起來

    # Auto determine feature number
    if not type(rocket_n_features) is int:
        if window_length <= 27:
            rocket_n_features = window_length*84
        elif window_length <= 360:
            rocket_n_features = 4956
        else:
            rocket_n_features = 9996
    
    # Select use rocket class
    rocket_used = None
    if is_mini == True:
        rocket_used = MiniRocket(num_features=rocket_n_features, 
                            max_dilations_per_kernel=32, 
                            random_state=random_state)
    else:
        rocket_used = Rocket(num_kernels=rocket_n_features/2, 
                    normalise=False, 
                    n_jobs=-1, 
                    random_state=random_state)
    
    # Run rocket
    X_transform_all = pd.DataFrame()
    transform_list = []    
    for col in rownames_list:
        X_transform = rocket_used.fit(X[[col]]).transform(X[[col]])
        X_transform.index = X[[col]].index
        transform_list.append(X_transform)
        
    X_transform_all = pd.concat(transform_list)# 增加多的col:下一個rolling window的feature
    X_transform_all.reset_index(inplace=True)
    X_transform_all.drop(columns=['index'], inplace=True)
    
    # Change column name
    if is_mini == True:
        X_transform_all.columns = [f"minirocket_feature_{i+1}" for i in range(rocket_n_features)]
    else:
        X_transform_all.columns = [f"rocket_feature_{i+1}" for i in range(rocket_n_features)]
    
    
    # Normalise(source code in sktime)
    X_transform_all = (X_transform_all - X_transform_all.mean()) / (X_transform_all.std() + 1e-8)

    return X_transform_all


def _talib_feature(df, column_id, column_time, column_target, win_len, talib_config):
    multi_result_name = {
        "BBANDS": [ "upperband", "middleband", "lowerband" ],
        "MAMA": [ "mama", "fama" ],
        "MACD": [ "macd", "macdsignal", "macdhist" ],
        "MACDEXT": [ "macd", "macdsignal", "macdhist" ],
        "STOCHRSI": [ "fastk", "fastd" ],
        "MINMAX": [ "min", "max" ],
        "MINMAXINDEX": [ "minidx", "maxidx" ],
        "HT_PHASOR": [ "inphase", "quadrature" ],
        "HT_SINE": [ "sine", "leadsine" ],    
    }    
    matype = [ "SMA", "EMA", "WMA", "DEMA", "TEMA", "TRIMA", "KAMA", "MAMA", "T3" ]
    ta_result = {}
    for _, group_df in df.groupby(column_id):
        time_series_data = group_df[column_target].astype(float).to_numpy()
        data_length = time_series_data.shape[0]
        for func_name in talib_config:
            func = getattr(ta, func_name)
            for params in talib_config[func_name]:
                if "timeperiod" in params:
                    params["timeperiod"] = win_len
                result = func(time_series_data, **params)
                params_name = "__".join([ "talib", func_name ] + [ str(k) + '_' + str(v) if k != "matype" else str(k) + '_' + str(matype[v]) for k, v in params.items() ])
                if len(result) == data_length:
                    if not params_name in ta_result:
                        ta_result[params_name] = np.array([])
                    ta_result[params_name] = np.append(ta_result[params_name], result)
                else:
                    for i in range(len(result)):
                        temp_name = params_name+"__"+multi_result_name[func_name][i]
                        if not temp_name in ta_result:
                            ta_result[temp_name] = np.array([])
                        ta_result[temp_name] = np.append(ta_result[temp_name], result[i])
    for ta_feat in ta_result:
        df[ta_feat] = pd.Series(ta_result[ta_feat].tolist())
    return df

def _tsfresh_features(df, column_id, column_time, column_target, window_length, tsfresh_config):
    result_df = []
    tsf_config, rename_list = _tsfresh_config_rename(tsfresh_config, window_length)
    for cate, group in df.groupby(column_id):
        feat = extract_features(group[['id', column_time, column_target]], column_id='id', column_sort=column_time, default_fc_parameters=tsf_config, n_jobs=32, disable_progressbar=True)
        feat.reset_index(inplace=True)
        feat.rename(columns={"level_0": column_id, "level_1": column_time}, inplace=True)
        cols = feat.columns.tolist()[2:]
        rename_dict = { col_name: new_name for col_name, new_name in zip(cols, rename_list) }
        feat.rename(columns=rename_dict, inplace=True)
        result_df.append(feat)
    return pd.concat(result_df)

def _tsfresh_config_rename(config, win_len):
    new_config = {}
    rename_list = []
    for func in config:
        new_config[func] = []
        for param in config[func]:
            temp_param = {}
            param_str = "tsfresh__w" + str(win_len) + "__" + func
            for k, v in param.items():
                if k.startswith("RELATIVE"):
                    new_k = k[9:]
                    new_v = round(v * win_len)
                    temp_param[new_k] = new_v
                    param_str += "__" + new_k + '_' + str(int(v*100)) + 'P'
                else:
                    temp_param[k] = v
                    param_str += "__" + k + '_' + str(v)
            new_config[func].append(temp_param)
            rename_list.append(param_str)
    return new_config, rename_list

def _filter_features(config):
    FEATURE_CATEGORY = get_feature_category()
    FEATURE_CATEGORY = { k: v for k, v in FEATURE_CATEGORY.items() if k in config['TAT_USE_CATEGORY']}
    
    all_funcs = []
    for category in FEATURE_CATEGORY:
        all_funcs += FEATURE_CATEGORY[category]
    pop_list = []
    for func in config['TALIB_CONFIG']:
        if not func in all_funcs:
            pop_list.append(func)
    for func in pop_list:
        config['TALIB_CONFIG'].pop(func)
    
    pop_list = []
    for func in config['TSFRESH_CONFIG']:
        if not func in all_funcs:
            pop_list.append(func)
    for func in pop_list:    
        config['TSFRESH_CONFIG'].pop(func)
    return config

def _rename_functions(column_names):
    FEATURE_CATEGORY = get_feature_category()
    inverted_index_dict = {}
    for category, functions in FEATURE_CATEGORY.items():
        for func in functions:
            inverted_index_dict[func] = category
    
    rename_function_dict = {}
    for col_name in column_names:
        if col_name.startswith("talib"):
            func_name = col_name.split("__")[1]
        elif col_name.startswith("tsfresh"):
            func_name = col_name.split("__")[2]
        else:
            func_name = None
        if func_name:
            if func_name in inverted_index_dict:
                rename_function_dict[col_name] = inverted_index_dict[func_name] + "__" + col_name
            else:
                rename_function_dict[col_name] = "Others__" + col_name
    return rename_function_dict

def get_feature_category():
    return {
            "Complexity": [
                "approximate_entropy", "binned_entropy", "fourier_entropy", "lempel_ziv_complexity", "permutation_entropy",
            ],
            "CycleIndicators": [
                "HT_DCPERIOD", "HT_DCPHASE", "HT_PHASOR", "HT_SINE", "HT_TRENDMODE",
            ],
            "LogicalCondition": [
                "index_mass_quantile", "number_cwt_peaks", "number_peaks", "ratio_beyond_r_sigma", "symmetry_looking", "value_count",
            ],
            "MathFunctions": [
                "ATAN", "COS", "COSH", "EXP", "LN", "LOG10", "SIN", "SINH", "SQRT", "TAN", "TANH", "MAX", "MIN", "SUM",
            ],
            "MomentumIndicators": [
                "APO", "CMO", "MACD", "MACDEXT", "MOM", "PPO", "ROC", "ROCP", "ROCR", "ROCR100", "RSI", "STOCHRSI", "TRIX",
            ],
            "MovingAverage":[
                "BBANDS", "DEMA", "EMA", "HT_TRENDLINE", "KAMA", "MAMA", "MIDPOINT", "SMA", "T3", "TEMA", "TRIMA", "WMA", 
            ],
            "StatisticFeatures": [
                "STDDEV", "VAR", "cid_ce", "energy_ratio_by_chunks", "large_standard_deviation", "change_quantiles", "quantile", 
                "matrix_profile", "spkt_welch_density", "time_reversal_asymmetry_statistic",
            ],
            "StatisticModels": [
                "LINEARREG", "LINEARREG_ANGLE", "LINEARREG_INTERCEPT", "LINEARREG_SLOPE", "TSF", "augmented_dickey_fuller", "ar_coefficient", 
                "agg_linear_trend", "autocorrelation", "c3", "cwt_coefficients", "fft_aggregated", "fft_coefficient", "friedrich_coefficients", 
                "linear_trend", "partial_autocorrelation", "max_langevin_fixed_point",
            ],
        }