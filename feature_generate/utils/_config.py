'''
config = {
    "COLUMN_ID": "category_id",
    "COLUMN_TIME": "tx_dt",
    "COLUMN_TARGET": "amount",
    "FG_WINDOW_SIZE_LIST": [
        12, 
        18
    ],
    "TAT_USE_CATEGORY": [
        "Complexity", 
        "CycleIndicators", 
        "LogicalCondition", 
        "MathFunctions", 
        "MomentumIndicators", 
        "MovingAverage", 
        "StatisticFeatures", 
        "StatisticModels"
    ],
    "FN_METHOD": "minmax"
}
'''

import talib as ta
import tsfresh as tsf

def check(variable, var_type, var_range=None, var_choice=None):
    if isinstance(variable, var_type):
        if var_range:
            if variable >= var_range[0] and variable <= var_range[1]:
                return True
            else:
                return False
        if var_choice:
            if variable in var_choice:
                return True
            else:
                return False
        return True
    return False

def check_config(api_config):
    error_message = []
    sep = ': '
    """
        Check for feature generate parameters.
    """
    if not check(api_config, dict):
        error_message.append(f"CONFIG{sep}TypeError{sep}feature_generate_config{sep}expect dict.")
        
    if not ('COLUMN_ID' in api_config and 
            'COLUMN_TIME' in api_config and 
            'COLUMN_X' in api_config):
        error_message.append(f"CONFIG{sep}KeyError{sep}feature_generate_config{sep}must have `COLUMN_ID`, `COLUMN_TIME`, `COLUMN_X`.")
    
    if 'FG_WINDOW_SIZE_LIST' in api_config:
        if not check(api_config['FG_WINDOW_SIZE_LIST'], list):
            error_message.append(f"CONFIG{sep}TypeError{sep}feature_generate_config{sep}FG_WINDOW_SIZE_LIST{sep}expect list.")
        else:
            if not True in [ check(num, int, var_range=[10, 1e9]) for num in api_config['FG_WINDOW_SIZE_LIST'] ]:
                error_message.append(f"CONFIG{sep}ValueError{sep}feature_generate_config{sep}FG_WINDOW_SIZE_LIST{sep}expect list of int and all value >= 10.")
    
    categories = ["Complexity", "CycleIndicators", "LogicalCondition", "MathFunctions", "MomentumIndicators", "MovingAverage", "StatisticFeatures", "StatisticModels"]
    if 'TAT_USE_CATEGORY' in api_config:
        if not check(api_config['TAT_USE_CATEGORY'], list):
            error_message.append(f"CONFIG{sep}TypeError{sep}feature_generate_config{sep}TAT_USE_CATEGORY{sep}expect list.")
        else:
            if not True in [ check(cate, str, var_choice=categories) for cate in api_config['TAT_USE_CATEGORY'] ]:
                error_message.append(f"CONFIG{sep}ValueError{sep}feature_generate_config{sep}TAT_USE_CATEGORY{sep}expect list of str and all value in {categories}.")
    
    if 'FN_METHOD' in api_config:
        if not check(api_config['FN_METHOD'], str, var_choice=["minmax", "standard"]):
            error_message.append(f"CONFIG{sep}ValueError{sep}feature_generate_config{sep}FN_METHOD{sep}expect `minmax` or `standard`.")
    
    return error_message

def set_config(custom_config, default_config):
    if custom_config == {}:
        custom_config = default_config
        return custom_config
    
    for k, v in custom_config.items():
        if k in default_config:
            default_config[k] = v
    custom_config = default_config
    return custom_config

def get_default_config():
    default_config = {}
    
    '''
        ##################################################
        Simple Config 
        - Recommend to change according to data time frequency.
        ##################################################
    '''
    # FeatureGenerator - General
    default_config['COLUMN_ID'] = "category_id"
    default_config['COLUMN_TIME'] = "tx_dt"
    default_config['COLUMN_X'] = "amount"
    default_config['FG_WINDOW_SIZE_LIST'] = [ 12, 18 ]
    
    # FeatureGenerator - Rocket
    default_config['RK_FEATURES'] = "auto"
    default_config['RK_USED_MINI'] = True
    default_config['RK_RANDOM_STATE'] = 43

    # FeatureGenerator - TA-Lib & Tsfresh
    default_config['TAT_USE_CATEGORY'] = [ "Complexity", "CycleIndicators", "LogicalCondition", "MathFunctions", 
                        "MomentumIndicators", "MovingAverage", "StatisticFeatures", "StatisticModels" ]

    # FeatureGenerator - Fill NA, +inf, -inf
    default_config['FNF_METHOD'] = "ffill"
    default_config['FNF_MIN_DATA_LENGTH'] = 0.7

    # FeatureGenerator - Feature Normalization
    default_config['FN_WINDOW_SIZE'] = 0
    default_config['FN_SHIFT_SIZE'] = 0
    default_config['FN_METHOD'] = "minmax"
    
    '''
        ##################################################
        Detail Config 
        - Recommend `NOT` change.
        ##################################################
    '''
    # FeatureGenerator - Ta-Lib
    # The param `timeperiod` will assign in function utils._feature_generate._talib_features.
    TALIB_NBDEV = [ 1, 2 ]
    TALIB_MATYPE = [ 1, 4, 7 ]
    default_config['TALIB_CONFIG'] = {
        'BBANDS': [ { 'timeperiod': None } ],
        'DEMA': [ { 'timeperiod': None } ],
        'EMA': [ { 'timeperiod': None } ],
        'HT_TRENDLINE': [{}],
        'KAMA': [ { 'timeperiod': None } ],
        'MAMA': [ { 'fastlimit': 0.1, 'slowlimit': 0.01 }, { 'fastlimit': 0.9, 'slowlimit': 0.09 } ],
        'MIDPOINT': [ { 'timeperiod': None } ],    
        'SMA': [ { 'timeperiod': None } ],
        'T3': [ { 'timeperiod': None } ],
        'TEMA': [ { 'timeperiod': None } ],
        'TRIMA': [ { 'timeperiod': None } ],
        'WMA': [ { 'timeperiod': None } ],
        'APO': [ { 'matype': m } for m in TALIB_MATYPE ],
        'CMO': [ { 'timeperiod': None } ],
        'MACD': [{}],
        'MACDEXT': [ { 'signalmatype': m } for m in TALIB_MATYPE ],
        'MOM': [ { 'timeperiod': None } ],
        'PPO': [ { 'matype': m } for m in TALIB_MATYPE ],
        'ROC': [ { 'timeperiod': None } ],
        'ROCP': [ { 'timeperiod': None } ],
        'ROCR': [ { 'timeperiod': None } ],
        'ROCR100': [ { 'timeperiod': None } ],
        'RSI': [ { 'timeperiod': None } ],
        'STOCHRSI': [{}] + [ { 'timeperiod': None, 'fastd_matype': m } for m in TALIB_MATYPE ],
        'TRIX': [ { 'timeperiod': None } ],
        'HT_DCPERIOD': [{}], 'HT_DCPHASE': [{}], 'HT_PHASOR': [{}], 'HT_SINE': [{}], 'HT_TRENDMODE': [{}],
        'LINEARREG': [ { 'timeperiod': None } ],
        'LINEARREG_ANGLE': [ { 'timeperiod': None } ],
        'LINEARREG_INTERCEPT': [ { 'timeperiod': None } ],
        'LINEARREG_SLOPE': [ { 'timeperiod': None } ],
        'STDDEV': [ { 'timeperiod': None, 'nbdev': n } for n in TALIB_NBDEV ],
        'TSF': [ { 'timeperiod': None } ],
        'VAR': [ { 'timeperiod': None, 'nbdev': n } for n in TALIB_NBDEV ],
        'ATAN': [{}], 'COS': [{}], 'COSH': [{}], 'EXP': [{}], 'LN': [{}], 'LOG10': [{}],
        'SIN': [{}], 'SINH': [{}], 'SQRT': [{}], 'TAN': [{}], 'TANH': [{}],
        'MAX': [ { 'timeperiod': None } ],    
        'MIN': [ { 'timeperiod': None } ],    
        'SUM': [ { 'timeperiod': None } ],
    }
    
    # FeatureGenerator - Tsfresh
    # Absolute value use as parameters
    PERCENTAGE_RANGE_HALF = [.1, .2, .3, .4]
    PERCENTAGE_RANGE_THIRD = [.33, .67]
    PERCENTAGE_RANGE_QUARTER = [.25, .5, .75]
    PERCENTAGE_RANGE_FIFTH = [.2, .4, .6, .8]
    PERCENTAGE_RANGE_TENTH = [.1, .2, .3, .4, .5, .6, .7, .8, .9]    
    THRESHOLD_RANGE = [.6, .7, .8, .9]
    default_config['TSFRESH_CONFIG'] = {
        "time_reversal_asymmetry_statistic": [{"RELATIVE_lag": lag} for lag in PERCENTAGE_RANGE_FIFTH],
        "c3": [{"RELATIVE_lag": lag} for lag in PERCENTAGE_RANGE_FIFTH],
        "cid_ce": [{"normalize": True}, {"normalize": False}],
        "symmetry_looking": [{"r": r} for r in PERCENTAGE_RANGE_FIFTH],
        "large_standard_deviation": [{"r": r} for r in PERCENTAGE_RANGE_FIFTH],
        "quantile": [{"q": q} for q in PERCENTAGE_RANGE_QUARTER],
        "autocorrelation": [{"RELATIVE_lag": lag} for lag in PERCENTAGE_RANGE_HALF],
        "agg_autocorrelation": [{"f_agg": s, "maxlag": 44} for s in ["mean", "median", "var"]],
        "partial_autocorrelation": [{"RELATIVE_lag": lag} for lag in PERCENTAGE_RANGE_HALF],
        "number_cwt_peaks": [{"RELATIVE_n": n} for n in PERCENTAGE_RANGE_HALF],
        "number_peaks": [{"RELATIVE_n": n} for n in PERCENTAGE_RANGE_HALF],
        "binned_entropy": [{"max_bins": max_bins} for max_bins in [5, 10, 30]],
        "index_mass_quantile": [{"q": q} for q in PERCENTAGE_RANGE_FIFTH],
        "cwt_coefficients": [{"widths": width, "RELATIVE_coeff": coeff, "w": w} 
                             for width in [(5, 10, 20)] for coeff in PERCENTAGE_RANGE_QUARTER for w in [5, 10, 20]],
        "spkt_welch_density": [{"RELATIVE_coeff": coeff} for coeff in PERCENTAGE_RANGE_HALF],
        "ar_coefficient": [{"RELATIVE_coeff": coeff, "RELATIVE_k": k} 
                           for coeff in PERCENTAGE_RANGE_HALF for k in PERCENTAGE_RANGE_THIRD if k >= coeff],
        "change_quantiles": [{"ql": ql, "qh": qh, "isabs": b, "f_agg": f}
                         for ql, qh in [[.0, .24], [.33, .66], [.44, .77], [.57, .87], [.64, .96], [.77, 1.], [.0, 1.]]
                         for b in [False, True] for f in ["mean", "var"]],
        "fft_coefficient": [{"RELATIVE_coeff": k, "attr": a} for a in ["real", "imag", "abs", "angle"] for k in PERCENTAGE_RANGE_HALF],
        "fft_aggregated": [{"aggtype": s} for s in ["centroid", "variance", "skew", "kurtosis"]],
        "value_count": [{"value": value} for value in [0, 1, -1]],
        "approximate_entropy": [{"m": 2, "r": r} for r in PERCENTAGE_RANGE_FIFTH],
        "friedrich_coefficients": (lambda m: [{"coeff": coeff, "m": m, "r": 30} for coeff in range(m + 1)])(3),
        "max_langevin_fixed_point": [{"m": 3, "r": 30}],
        "linear_trend": [{"attr": "pvalue"}, {"attr": "rvalue"}, {"attr": "intercept"},
                         {"attr": "slope"}, {"attr": "stderr"}],
        "agg_linear_trend": [{"attr": attr, "RELATIVE_chunk_len": i, "f_agg": f}
                             for attr in ["rvalue", "intercept", "slope", "stderr"]
                             for i in PERCENTAGE_RANGE_THIRD
                             for f in ["max", "min", "mean", "var"]],
        "augmented_dickey_fuller": [{"attr": "teststat"}, {"attr": "pvalue"}, {"attr": "usedlag"}],
        "energy_ratio_by_chunks": [{"num_segments": 10, "segment_focus": i} for i in range(10)],
        "ratio_beyond_r_sigma": [{"r": x} for x in [0.01, 0.1, 0.5, 2, 2.5, 3, 7, 10]], 
        "lempel_ziv_complexity": [{"RELATIVE_bins": x} for x in PERCENTAGE_RANGE_FIFTH],
        "fourier_entropy":  [{"RELATIVE_bins": x} for x in PERCENTAGE_RANGE_FIFTH],
        "permutation_entropy":  [{"tau": t, "RELATIVE_dimension": d} for t in [1, 4, 7] for d in PERCENTAGE_RANGE_QUARTER],
        "matrix_profile": [{"threshold": t, "feature": f} for f in ["min", "max", "mean", "median", "25", "75"] for t in THRESHOLD_RANGE],
        "count_below": [{"t": t} for t in [10.0, 20.0]],
    }
    return default_config
