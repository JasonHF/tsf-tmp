import pandas as pd
import copy

#預設吃進來的是spring0生成的特徵表格，是已經做好種類和日期排序的
def _min_max_normalize(series, return_all = False):
    """
    Input:
    series: list or pandas.Series
    
    Output:
    the normalized value of the last item in series
    """
    series = list(series)
    MIN = min(series)
    MAX = max(series)
        
    if return_all == False:
        if MIN == MAX:
            return 0
        else:
            return float((series[-1] - MIN) / (MAX - MIN))
    else:
        if MIN == MAX:
            return [0 for scalar in series]
        else:
            return [float((scalar - MIN) / (MAX - MIN)) for scalar in series]

def _standard_normalize(series, return_all = False):
    """
    Input:
    series: list or pandas.Series
    
    Output:
    the normalized value of the last item in series
    """
    series = list(series)
    N = len(series)
    MEAN = sum(series)/N
    SE = (sum([(scalar - MEAN)**2 for scalar in series])/(N-1))**(1/2)
        
    if return_all == False:
        if SE == 0:
            return 0
        else:
            return float((series[-1] - MEAN) / (SE))
    else:
        if SE == 0:
            return [0 for scalar in series]
        else:
            return [float((scalar - MEAN) / (SE)) for scalar in series]


def feature_normalize(df, column_id, column_time, column_filter_list = [], window_length = 0, shift = 0, method = "minmax"):
    """
    df:pd.DataFrame
    column_id:str= the column name of category
    column_time:str= the column name of datetime
    column_filter_list = the list of column names which are not used in normalization
    window_length:int = 0 for not using rolling window, ohters for specifying the length of rolling window
    shift:int = the shift between rolling window and normalized value 
    method:int= 1 for standard normalize; 0 for min max normalize
    """
    category_list = sorted(list(set(df[column_id])))
    number_of_datetime = df.shape[0]//len(category_list)
    time_list = list(df[column_time])[:number_of_datetime]
    normalize_method = _standard_normalize if method == "standard" else _min_max_normalize

    if (type(window_length)!=int) or (type(shift)!=int):
        raise TypeError("window_length and shift must be type of int")
    if window_length == 0:
        window_length = number_of_datetime
    if window_length > number_of_datetime:
        raise BaseException(f"rolling window length must be less than or equal to the number of datetime: {number_of_datetime}, but we got window_length = {window_length}")
    if window_length < shift :
        raise BaseException(f"rolling window length must be larger than or equal to the number of shift: {shift}, but we got window_length = {window_length}")

        
    # rocket feature也不需要正規化
    rocket_column_list = [c for c in df.columns if "rocket" in c]
    column_filter_list.extend([column_time,column_id])
    column_filter_list.extend(rocket_column_list)
    column_filter_list = column_filter_list[::-1]
    df_normalized_list = []
    for category in category_list:
        df_tmp = df[df[column_id] == category]
        # 刪掉不要正規化的columns
        df_tmp = df_tmp.drop(columns = column_filter_list)
        df_tmp_copy = copy.deepcopy(df_tmp)
        # 逐欄做正規化
        for feature_column in list(df_tmp.columns):
            if window_length == number_of_datetime: # 當window_length為0時，不做rolling
                X = df_tmp[feature_column]
                MAVG = X.mean()
                MSTD = X.std()
                MSTD = MSTD if MSTD != 0 else 1
                MMIN = X.min()
                MMAX = X.max()
            else:
                X = df_tmp[feature_column]
                MAVG = X.rolling(window = window_length, min_periods = 0).mean().shift(periods=shift, fill_value = 0)
                MSTD = X.rolling(window = window_length, min_periods = 0).std().shift( periods=shift, fill_value = 0)
                MSTD = MSTD.fillna(1).replace(to_replace=0, value=1) #標準差的NA和0都補成1
                MMIN = X.rolling(window = window_length, min_periods = 0).min().shift( periods=shift, fill_value = 0)
                MMAX = X.rolling(window = window_length, min_periods = 0).max().shift( periods=shift, fill_value = 0)
                
            if method == "standard":
                df_tmp[feature_column] = (X - MAVG) / MSTD
            else:
                df_tmp[feature_column] = (X - MMIN) / (MMAX - MMIN)
                
        # 補回被shift掉的值: 用首shift長度個的欄位值做正規化
        if shift >= 1:
            df_tmp.iloc[:shift+1, :] = df_tmp_copy.iloc[:shift+1,:].apply(normalize_method, return_all = True)
        
        # 補回篩除掉的columns
        df_tmp = pd.concat([df_tmp, df[df[column_id] == category][column_filter_list]], axis = 1)
        # 一個類別做好了，加入最終的df
        df_normalized_list.append(df_tmp)
    
    df_normalized = pd.concat(df_normalized_list, axis = 0)
    df_normalized = df_normalized.fillna(0) #在rolling過程可能有NA
    df_normalized.reset_index(drop = True, inplace = True)
    return df_normalized